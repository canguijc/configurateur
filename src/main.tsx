import React from 'react'
import ReactDOM from 'react-dom/client'

import { Provider } from 'react-redux'
import {store} from './store/store'

import './store/features/translate/i18n'

import App from './App'

// style
import './index.scss'

// fonts
import './fonts/Forza-Bold.ttf'
import './fonts/Roboto-Regular.ttf'
import './fonts/Forza-Book.ttf'
import './fonts/Forza-Medium.ttf'
import './fonts/Roboto-Regular.ttf'
import './fonts/Roboto-Italic.ttf'
import './fonts/Roboto-Bold.ttf'
import './fonts/Roboto-Medium.ttf'


ReactDOM.createRoot(document.getElementById('configurateur') as HTMLElement).render(
      <Provider store={store}>
        <App />
      </Provider>
    
)
