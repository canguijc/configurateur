import { Navigate } from "react-router-dom"
import { useAppDispatch, useAppSelector } from "../../../store/hooks/reduxHooks"
import { clearEngineSettings, selectGamme } from '../../../store/features/engineSetting/engineSettingSlice'
import Models from "./components/models/Models"
import appConfig from "../../../appconfig"
import useAppClearStore from "../../../store/hooks/useAppClearStore"

/** show list of models if a gammme has been selected
 *  wrapper to not declare  a hook conditionnaly
 * 
 */
const ModelList = () => {
    const dispatch = useAppDispatch()
    const clearAppStore = useAppClearStore()
    const currentGamme = useAppSelector(selectGamme)
    const gammesPageLink = appConfig.pages.gammes.link.abs

    //  test if a gamme has been selected
    if (currentGamme){
        return (<Models currentGamme={currentGamme} />)
    } else {
        // no gamme have been selected
        clearAppStore()
        return(
            <Navigate to={gammesPageLink}/>
        )
    }
}

export default  ModelList