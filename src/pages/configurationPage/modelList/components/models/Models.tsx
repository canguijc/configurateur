
// style
import { useTranslation } from "react-i18next"
import { Navigate, useNavigate } from "react-router-dom"
import appConfig from "../../../../../appconfig"
import SectionTitle from "../../../../../components/sectionTitle/SectionTitle"
import CustomBtn from "../../../../../components/ui/button/customBtn/CustomBtn"
import LoadingSpinner from "../../../../../components/ui/spinner/LoadingSpinner"
import ModelListTable from "../../../../../components/ui/table/modelListTable/ModelLisTable"
import { useGetAllModelApiQuery } from "../../../../../store/features/api/apiSlice"
import useAppClearStore from "../../../../../store/hooks/useAppClearStore"
import { clearEngineSettings, selectGamme, selectModel, setTestMode } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { Gamme } from "../../../../../store/features/gamme/gammesData"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import "./models.scss"

export interface ModelsProps {
    currentGamme: Gamme
}
const Models = ({currentGamme}: ModelsProps) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const clearAppStore = useAppClearStore()
    const currentModel = useAppSelector(selectModel)
    const pages = appConfig.pages
    let models = undefined
    let content

    //  test if a gamme has been selected
     models = useGetAllModelApiQuery(currentGamme.reference)
    // go back a step
    const handleclickStepBack = () => {
        // remove the selected model and the selected gamme and go back to the gamme page
        clearAppStore()
        navigate(pages.gammes.link.abs)
    }

    // validate this step and go to the next
    const handleValidate = () => {
        navigate(pages.options.link.abs)
    }


    // data loading not finish yet ...
    if (models?.isLoading) {
        content = (
               <div 
                className="scroll-table"
               >
                    <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                        <LoadingSpinner/>
                    </div>
               </div> 
        )
    // data is ready
    } else if (models?.isSuccess) {
        // temporary mod 
        models.data.tmp && dispatch(setTestMode(models.data.tmp))
       content = ( 
            <div  className="scroll-table px-2">
                <ModelListTable models={models.data.models}/> 
            </div>
        )
    // error while fetching models data
    } else if (models?.isError) {
        // content = <Navigate to="/"/>
        content = <div> la requête a échoué </div>
    }

    return ( 
        <div>
            {/* section title */}
            <SectionTitle 
            title={t("cfg_mod_label")}
            align='left'
            />
            {/* display model content or */} 
            {content}
            {/* navigate to step */}
            <div className="d-flex justify-content-between my-3">
                {/* navigate previous step */}
                <CustomBtn
                    btnType='outline'
                    label= {t("cfg_back").toUpperCase()}
                    onClick={handleclickStepBack}
                />
                {/* validate  and go to next step */}
                <CustomBtn
                    label={t('cfg_val_btn').toUpperCase()}
                    onClick={handleValidate}
                    disabled={ !currentModel  ? true: false }
                />
            </div>

        </div>
    )
}

export default Models