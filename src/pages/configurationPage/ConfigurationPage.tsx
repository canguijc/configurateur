import { Navigate, Outlet, useLocation } from "react-router-dom"
import { useTranslation } from "react-i18next"
import { useAppDispatch, useAppSelector } from "../../store/hooks/reduxHooks"

import { selectGamme, selectModel } from "../../store/features/engineSetting/engineSettingSlice"

import { showPopup } from "../../store/features/modalpopup/modalPopupSlice"
import SelectedOptionTable from "../../components/ui/table/selectedOptionTable/SelectedOptionTable"
import SectionTitle from "../../components/sectionTitle/SectionTitle"
import { CancelConfLink } from "../../components/ui/link/CustomLink/customLink"
import CustomModal from "../../components/ui/modal/customModal/CustomModal"
import CancelConf from "../../components/ui/modal/_components/cancelConf/CancelConf"
import appConfig from "../../appconfig"

// style
import "./configurationPage.scss"

const ConfigurationPage = () => {
    const {t} = useTranslation()
    const dispatch = useAppDispatch()
    const currentGamme = useAppSelector(selectGamme) 
    const currentModel = useAppSelector(selectModel)
    const pathname = useLocation().pathname
    const pages = appConfig.pages

    // id of the modal
    const cancelConfModalId = 'confPagecancelConfModal' 
    const modelImageModalId = 'modelImageModalId'
    const isOnOptionPage = (pathname == pages.options.link.abs)
    const isOnModelPage = (pathname == pages.models.link.abs)

    //  show modal confirmation to delete configuration process and go back / 
    const handleBackLink = () => {
        // activate the  
        dispatch(showPopup(cancelConfModalId))
    }

    // back home if no gamme are selected
    if (!currentGamme) {
        return (<Navigate to={pages.gammes.link.abs} />)
    }

    return (
        <div className="px-2">
            {/* Modal used with Backlink to clear conf and go back to / */}
            <CustomModal 
                id={cancelConfModalId}
                title={t("cfg_mod_info")}
                actionsHandledInChildren={true}
            >
                <CancelConf/>
            </CustomModal>

            {/* Modal to display Model Image */}
                <CustomModal 
                    id={modelImageModalId}
                    title={currentModel ? currentModel.label : t("cfg_mod_info")}
                    hideButton={true}
                >
                    <div className="d-flex justify-content-center mb-2">
                        <img className="img-fluid" src={currentGamme?.image} alt={currentGamme?.alt} />
                    </div>
                </CustomModal>

            <div className="container">
                <div className="row mx-0">
                    {/* left: picture  */}
                    <div className="col-6">
                            {/* picture and title container */}
                                {/* section title */}
                                <SectionTitle title={currentGamme?.gamme || "" } align="left">
                                    {/* only visible on options route */}
                                    {/* this component clear the configuration but need to be link
                                        with  <CancelConfModal/>
                                    */}
                                    { isOnOptionPage &&
                                        <CancelConfLink
                                            text={`(${t('cfg_chan_modele')})`}
                                            onClick={handleBackLink}
                                        />
                                    }
                                </SectionTitle>
                                {/* model picture  */} 
                                { isOnModelPage &&
                                    <div className="d-flex justify-content-center">
                                        <img className="img-fluid" src={currentGamme?.image} alt={currentGamme?.alt} />
                                    </div>
                                }
                                <SelectedOptionTable
                                    modelStyle={isOnOptionPage ? {border:'border-bottom-thick'} : undefined}
                                    modelImageModalId={isOnOptionPage ? modelImageModalId : undefined }
                                />
                    </div>
                        
                    {/* right: modele list  */}
                    <div className="col-6">
                                {/* model table */}
                                <Outlet/>
                                {/* navigate to step */}
                    </div>
                </div>
            </div>
{/*
            {!isOnModelPage &&
*/}
            <div className="annotation pt-4">
                {t("cfg_mention1")}<br/>{t("cfg_mention2")}
            </div>
{/*
            }
*/}
        </div>
    )
   
}

export default  ConfigurationPage