import { Navigate } from "react-router-dom"
import appConfig from "../../../appconfig"
import { selectGamme, selectModel } from "../../../store/features/engineSetting/engineSettingSlice"
import { useAppDispatch, useAppSelector } from "../../../store/hooks/reduxHooks"
import useAppClearStore from "../../../store/hooks/useAppClearStore"
import OptionsList from "./components/optionsList/OptionsList"


/** show list of options if a gammme and a model have been selected
 *  wrapper to not declare  a hook conditionnaly
 * 
 * 
 */
const OptionServiceList = () => {
    const currentModel = useAppSelector(selectModel)
    const currentGamme = useAppSelector(selectGamme)
    const clearAppStore = useAppClearStore()
    const gammesPageLink = appConfig.pages.gammes.link.abs

     
    if (currentGamme && currentModel) {
        return (<OptionsList currentModel={currentModel}/>)

    } else {
        // no  Gamme or model  selected
        clearAppStore()
        return(
            <Navigate to={gammesPageLink}/>
        )
    }

}

export default  OptionServiceList