import { useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { useNavigate } from "react-router-dom"
import appConfig from "../../../../../appconfig"
import SectionTitle from "../../../../../components/sectionTitle/SectionTitle"
import CustomBtn from "../../../../../components/ui/button/customBtn/CustomBtn"
import DropUpButton from "../../../../../components/ui/button/dropUpButton/DropUpButton"
import LoadingSpinner from "../../../../../components/ui/spinner/LoadingSpinner"
import OptionListTable from "../../../../../components/ui/table/optionListTable/OptionLisTable"
import { Model, useGetOptionsByModelApiQuery } from "../../../../../store/features/api/apiSlice"
import { selectOptionStatusNecessity, setTestMode } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"

// style 
import "./optionsList.scss"

export interface OptionsListProps {
    currentModel: Model
}
const OptionsList = ({currentModel}:OptionsListProps ) => {
    const {t} = useTranslation()
    // current selected category. default on first category 
    const [currentOptionCategory, setCurrrentOptionCategory] = useState<string | null>(null)
    const currentLang = useAppSelector(selectCurrentLanguage)
    const necessities = useAppSelector(selectOptionStatusNecessity ) 
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const resumePageLink = appConfig.pages.resume.link.abs
    let options: any 
    let content

    // handle the selection of another category in dropdown button
    const handleSelectCategory = (ref: string) => {
        setCurrrentOptionCategory(ref)
    }

     

    options = useGetOptionsByModelApiQuery(currentModel.ref, {
        selectFromResult: ({data, isFetching, isLoading, isError, isSuccess}) => ({
            data: data,
            isFetching: isFetching,
            isError: isError,
            isLoading: isLoading,
            isSuccess: isSuccess,
            defaultCategory: data?.categoryRank[0],
            lastGroupRef: data && data.categoryRank  ? (data.categoryRank.slice(-1)).toString()  : null,
            nextCategoryRef:(currentRef: string): string | null => {
                let currentItemIndex: number | null = null

                if (data){
                    currentItemIndex = data.categoryRank.findIndex(el => el == currentRef)
                }
                // next item if not the last else return the first element of dataArray 
                if ( data && typeof currentItemIndex === "number" ) {
                    const nextItemIndex =  (currentItemIndex + 1) % data.categoryRank.length
                    return data.categoryRank[nextItemIndex]
                }
                return null
            },
            // filter to get options for a given category
            // default value  data.categoryRank[0]
            optionsByCategoryDefault: data?.list.filter(el => el.gp_opt ==  data.categoryRank[0]),
            optionsByCategoryRef: ( catRef: string ) =>  {
               const options = data?.list.filter(el => el.gp_opt == catRef) 
               let optionsSorted = options &&  options.sort((cur, prev) => cur.ref < prev.ref ? -1 : 1 )
               return optionsSorted ? optionsSorted : options 
            }
        })
    })

    // set test Mode
    useEffect(() => {
        if (options.isSuccess){
            options.data.tmp &&  dispatch(setTestMode(options.data.tmp))
        }
    }, [options])

    // alert User on refresh or page change
    // https://plainenglish.io/blog/how-to-alert-a-user-before-leaving-a-page-in-react
    // https://developer.mozilla.org/fr/docs/Web/API/Window/beforeunload_event

    // useEffect(() => {
    // window.addEventListener('beforeunload', alertUser)
    // return () => {
    //     window.removeEventListener('beforeunload', alertUser)
    // }
    // }, [])

    // const alertUser = (e:any) => {
    //     let confirmationMessage = "\o/";
    //     e.preventDefault()
    //     e.returnValue = ''
    // }



  // data loading not finish yet ...
    if (options?.isLoading) {
        content = (
               <div style={{height:"30vh"}}>
                    <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                        <LoadingSpinner/>
                    </div>
               </div> 
        )
    // error while fetching models data
    } else if ( options?.isError){
        // content = <Navigate to="/"/>
        content = <div> the request failed </div>
    } else if (options?.isSuccess) {

        const handleNextCategory = () => {
        /* here select the next option in options list*/
            if( currentOptionCategory ) {
                const nextRef = options.nextCategoryRef( currentOptionCategory)
                setCurrrentOptionCategory(nextRef)
            } 
            // first category to show
            else {
                const nextRef = options.nextCategoryRef( options.defaultCategory)
                setCurrrentOptionCategory(nextRef)
            } 
        }

        const handleResumeConfig = () => {
            navigate(resumePageLink)
        }

        content = (
            <>
                {/* section title */}
                    {/* if no current category selected, default to the first one */}
                    <SectionTitle 
                        title={options.data.categoryOptions[currentOptionCategory || options.defaultCategory][currentLang]} 
                        align='left'
                    />
                <div className="scroll-table px-2">
                    {/* model table */}
                    <OptionListTable
                        options={options.optionsByCategoryRef(currentOptionCategory || options.defaultCategory)}
                        modelRef={currentModel.ref}
                    />
                </div>

                    {/* navigate to step */}
                <div className="d-flex justify-content-between my-3">
                    <DropUpButton
                        categoryRank={options.data ? options.data.categoryRank : []}
                        categoryOptionLabel={options.data.categoryOptions}
                        currentOptionCategory={currentOptionCategory || options.defaultCategory}
                        handleSelectCategory= {handleSelectCategory}
                        handleSelectResumeConfig={handleResumeConfig}
                        />
                    
                    {/* loop on all categories btn */}
                    { currentOptionCategory !== options.lastGroupRef &&
                        <CustomBtn
                            btnType='plain'
                            // set the label of the next category
                            label ={options.data &&  currentOptionCategory  
                                ? options.data.categoryOptions[options.nextCategoryRef( currentOptionCategory)][currentLang] 
                                : options.data.categoryOptions[options.nextCategoryRef( options.defaultCategory)][currentLang]}
                            onClick={handleNextCategory}
                        />
                    }

                    {/* resume config btn */}
                    { currentOptionCategory === options.lastGroupRef &&
                        <CustomBtn
                            btnType='plain'
                            // set the label of the next category
                            label = {t('cfg_resume_config')}
                            onClick={handleResumeConfig}
                            disabled={necessities.length > 0 ? true : false}
                        />
                    } 
                </div>
            </>
        )
    }

    return (
        <div>
            {content}
        </div> 
    )
}

export default OptionsList