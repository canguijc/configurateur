import { useTranslation } from 'react-i18next'

import { useAppSelector } from '../../store/hooks/reduxHooks'
import { getAllGammes } from '../../store/features/gamme/gammeSlice'

import ConfiguratorStep from '../../components/configurationStep/ConfigurationStep'
import GammeCard from '../../components/gammeCard/GammeCard'

const GammePage = () => {
    const {t} = useTranslation()
    // get the differents  gammes 
    const gammes = useAppSelector(getAllGammes)

    return (
      <div className="container">
        <div className="row my-2">
            { gammes &&  gammes.map( (gamme, index) => (
              <div key={index} className="col-12 col-sm-6 col-md-4 my-4 ">
                <GammeCard  
                  gamme={gamme}
                />
              </div>
              ))
            }
        </div>
      </div>
    )
}

export default GammePage