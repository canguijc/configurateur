import { useTranslation } from "react-i18next"
import { useLocation, useNavigate } from "react-router-dom"
import { useAppDispatch } from "../../../../store/hooks/reduxHooks"

import { showPopup } from "../../../../store/features/modalpopup/modalPopupSlice"
import { Model } from "../../../../store/features/api/apiSlice"
import { Gamme } from "../../../../store/features/gamme/gammesData"

import SectionTitle from "../../../../components/sectionTitle/SectionTitle"
import CustomBtn from "../../../../components/ui/button/customBtn/CustomBtn"
import { CancelConfLink } from "../../../../components/ui/link/CustomLink/customLink"
import CustomModal from "../../../../components/ui/modal/customModal/CustomModal"
import CancelConf from "../../../../components/ui/modal/_components/cancelConf/CancelConf"
import SaveConfiguration from "../../../../components/ui/modal/_components/saveConfiguration/SaveConfiguration"
import SelectedOptionTable from "../../../../components/ui/table/selectedOptionTable/SelectedOptionTable"

import appConfig from "../../../../appconfig"
import {useEffect, useState} from "react";

export interface ResumePageContentProps {
    gamme:Gamme
    model: Model
}


const ResumePageContent = ({gamme, model}: ResumePageContentProps) => {

    const  {t} = useTranslation()
    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const pathname = useLocation().pathname
    const gammesPageLink = appConfig.pages.gammes.link.abs
    const optionsPageLink = appConfig.pages.options.link.abs
    const cancelConfModalId= 'resumePagecancelConfModal' 
    const saveConfigModalId = `saveConfigModal-${model.ref}`

    const handleBackLink = () => {
        // activate the  
        dispatch(showPopup(cancelConfModalId))
    }
    const handleMakeOffer = () => {
        dispatch(showPopup(saveConfigModalId))
    }
    const handleBackOptions = () => {
        navigate(optionsPageLink)
    }

    return (
        <div className="px-2">
            {/* Modal used with Backlink to clear conf and go back to / */}
            <CustomModal 
                id={cancelConfModalId}
                title={t("cfg_mod_info")}
                actionsHandledInChildren={true}
            >
                <CancelConf/>
            </CustomModal>

            {/* modal to save configuration */}
            <CustomModal
                id={saveConfigModalId}
                title={t('cfg_config_done')}
                actionsHandledInChildren={true}
            >
                <SaveConfiguration
                    model={model}
                />
            </CustomModal>

            <div className="container">
                <div className="row mx-0">
                    {/* left: picture  */}
                    <div className="col-4 mt-5">
                        {/* model picture  */} 
                        <div className="d-flex justify-content-center mt-4">
                            <img className="img-fluid" src={gamme.image} alt={gamme?.alt} />
                        </div>
                    </div>
                    {/* config data */}
                    <div className="col-8">

                        <SectionTitle 
                            title={gamme.gamme} 
                            align="left"
                            border={false}
                        >
                            {/* this component clear the configuration but need to be link
                                with  <CancelConfModal/>
                            */}
                                <CancelConfLink
                                    text={`(${t('cfg_chan_modele')})`}
                                    onClick={handleBackLink}
                                />
                        </SectionTitle>
                        <div className="mb-5">
                            <SelectedOptionTable/>
                        </div>

                        {/* btn go back options or make an offer */}
                        <div className="d-flex my-5 px-4 justify-content-between">
                            <CustomBtn
                                btnType='outline'
                                // set the label of the next category
                                label = {`< ${t('cfg_opt')}`}
                                onClick={handleBackOptions}
                            />

                            <CustomBtn
                                label={t('cfg_faire_offr_btn')}
                                onClick={handleMakeOffer}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="annotation pt-4">
                {t("cfg_mention1")}<br/>{t("cfg_mention2")}
            </div>

        </div>
    )
}

export default ResumePageContent