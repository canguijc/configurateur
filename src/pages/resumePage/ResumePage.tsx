
//style
import { Navigate } from 'react-router-dom'
import appConfig from '../../appconfig'
import { selectGamme, selectModel } from '../../store/features/engineSetting/engineSettingSlice'
import { useAppSelector } from '../../store/hooks/reduxHooks'
import useAppClearStore from '../../store/hooks/useAppClearStore'
import ResumePageContent from './components/resumePageContent/ResumePageContent'
import './resumePage.scss'

const ResumePage = () => {
    const currentGamme = useAppSelector(selectGamme) 
    const currentModel = useAppSelector(selectModel) 
    const clearAppStore = useAppClearStore()
    const gammesPageLink = appConfig.pages.gammes.link.abs


    if (currentGamme && currentModel) {
        return (<ResumePageContent gamme={currentGamme} model={currentModel}/>)
    } else {
        clearAppStore()
        return(<Navigate to={gammesPageLink}/>)
    }
}

export default ResumePage