import { useEffect } from "react"
import { clearEngineSettings, setGamme } from "../../../../store/features/engineSetting/engineSettingSlice"
import gammesData from "../../../../store/features/gamme/gammesData"
import { setRetrieveConfigurationStatus, setRetrieveError } from "../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch } from "../../../../store/hooks/reduxHooks"

export interface RetrieveGammeProps {
    gammeRef: string
}
const RetrieveGamme = ({gammeRef}:RetrieveGammeProps) => {
    const dispatch = useAppDispatch()

    useEffect(() => {
         const savedGamme =  gammesData.find(gamme => gamme.reference === gammeRef)
        //  if a gamme is found the add it to engineSettings
        if (savedGamme ) {
            dispatch(setGamme(savedGamme))
            dispatch(setRetrieveConfigurationStatus("canCheckModel"))
        } else {
            // error
            dispatch(clearEngineSettings())
            dispatch(setRetrieveError("la gamme n'existe pas"))
            dispatch(setRetrieveConfigurationStatus("failed"))
        }
    }, [])

    return  null
}

export default RetrieveGamme