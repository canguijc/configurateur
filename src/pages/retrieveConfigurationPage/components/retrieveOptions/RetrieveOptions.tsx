import LoadingSpinner from "../../../../components/ui/spinner/LoadingSpinner"
import { SaveConfigurationData, useGetOptionsByModelApiQuery } from "../../../../store/features/api/apiSlice"
import { setRetrieveConfigurationStatus, setRetrieveError } from "../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch } from "../../../../store/hooks/reduxHooks"
import TestOptions from "./_components/testOptions/TestOptions"

export interface RetrieveOptionsProps {
    optionsListRef: string[]
    modelRef: string
    configData: SaveConfigurationData
} 
const RetrieveOptions = ({modelRef, optionsListRef, configData}: RetrieveOptionsProps) => {
    const dispatch = useAppDispatch()
    const optionsModel = useGetOptionsByModelApiQuery(modelRef, {
        selectFromResult: ({data, isLoading, isError, isSuccess, isUninitialized}) => ({
            data: data,
            isUninitialized,
            isError,
            isLoading,
            isSuccess,
            // filter to get options for a given category
            // default value  data.categoryRank[0]
            optionsNotInListRef: (savedOptionsList: string[]): string[] | undefined => {
                if (data) {
                    const modelOptionsList = data.list.map(el => el.ref)
                    const savedOptionsNotInList = savedOptionsList.filter(savedOpt => !modelOptionsList.includes(savedOpt)) 
                    return savedOptionsNotInList
                }
            },
            // find incompatibilities between options
            optionsWithIncompatibilities: (savedOptionsList: string[]): string[] | undefined => {
                let incompatibilitiesListRef: string[] = []
                let optionsWithIncompatibilities: string[] = []
                if (data) {
                    savedOptionsList.forEach(savedOptionRef => {
                       const foundOption =  data.list.find(option => option.ref === savedOptionRef)
                        if (foundOption && foundOption.constraint && foundOption.constraint.incomp) {
                            incompatibilitiesListRef = [...incompatibilitiesListRef, ...foundOption.constraint.incomp ]
                        }
                    })
                    // remove duplicate
                    incompatibilitiesListRef = [...new Set(incompatibilitiesListRef)]
                    // options with incompatibilities
                    optionsWithIncompatibilities = savedOptionsList.filter(savedOpt => incompatibilitiesListRef.includes(savedOpt)) 
                    return [...new Set(optionsWithIncompatibilities)]
                }
            }
        })
    })
    let content: any
    if(optionsModel.isError){
        dispatch(setRetrieveError('erreur lors du chargement des options du modèle'))
        dispatch(setRetrieveConfigurationStatus('failed'))
        content = <div>erreur lors du chargement...</div>

    } else if (optionsModel.isUninitialized) {
        content = <div></div>

    } else if (optionsModel.isLoading){
        content =(
            <div>
                <div>
                    chargement des options...
                    <LoadingSpinner/>
                </div>
            </div>
        ) 

    } else if (optionsModel.isSuccess){
       
        content = (
            <div>
                { optionsModel.data && 
                    <TestOptions
                        configurationOptionsList={optionsListRef}
                        configData={configData}
                        optionsIncompatibility={optionsModel.optionsWithIncompatibilities(optionsListRef)}
                        optionsMissing={optionsModel.optionsNotInListRef(optionsListRef)}
                        modeloptionsList = {optionsModel.data?.list}
                    />
                }
            </div>
        )
    }
    return (<div>{content}</div>)
}

export default RetrieveOptions