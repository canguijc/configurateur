import { memo, useEffect } from "react"
import { OptionRowTransformed, SaveConfigurationData } from "../../../../../../store/features/api/apiSlice"
import { addOrRemoveOption } from "../../../../../../store/features/engineSetting/engineSettingSlice"
import { setRetrieveConfigurationData, setRetrieveConfigurationStatus } from "../../../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch } from "../../../../../../store/hooks/reduxHooks"

export interface PopulateOptionsProps {
    configurationOptionsList: string[]
    modelOptionsList:OptionRowTransformed[]
    configData: SaveConfigurationData 
    
}
const PopulateOptions = ({configurationOptionsList, modelOptionsList, configData}: PopulateOptionsProps) => {
    const dispatch = useAppDispatch()
   useEffect( () => {
    configurationOptionsList.forEach(optionRefToAdd => {
        const optionInList = modelOptionsList.find(modelOption => modelOption.ref === optionRefToAdd)
        optionInList && dispatch(addOrRemoveOption(optionInList))
    })  
    // dispatch(setRetrieveConfigurationData(configData))
    dispatch(setRetrieveConfigurationStatus('succeeded'))
   }, [])

    return null

}

export default PopulateOptions