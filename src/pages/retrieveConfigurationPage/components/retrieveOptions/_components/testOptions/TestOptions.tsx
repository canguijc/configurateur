import { useEffect } from "react"
import { OptionRowTransformed, SaveConfigurationData } from "../../../../../../store/features/api/apiSlice"
import { selectRetrieveConfiguration, setRetrieveConfigurationStatus, setRetrieveConfOptionsNotInList, setRetrieveConfOptionsWithIncompatibility } from "../../../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch, useAppSelector } from "../../../../../../store/hooks/reduxHooks"
import PopulateOptions from "../populateOptions/PopulateOptions"

export interface TestOptionsProps {
    configurationOptionsList: string[]
    modeloptionsList: OptionRowTransformed[]
    optionsIncompatibility: string[] | undefined
    optionsMissing: string[] | undefined
    configData: SaveConfigurationData
}

const TestOptions = ({optionsIncompatibility, optionsMissing, configurationOptionsList, modeloptionsList, configData}: TestOptionsProps)=> {

    const dispatch = useAppDispatch()
    // const hasIncompatibility = useMemo()
    const retrieveConfiguration = useAppSelector(selectRetrieveConfiguration)

    useEffect(() => {
        let OptionsHaveIncompabilities = optionsIncompatibility && optionsIncompatibility.length > 0
        console.log("OptionsHaveIncompabilities", OptionsHaveIncompabilities)
        let OptionsAreMissing = optionsMissing && optionsMissing.length > 0
        console.log("OptionsareMissing", OptionsAreMissing)

        if (OptionsHaveIncompabilities) {
            dispatch(setRetrieveConfOptionsWithIncompatibility({list:optionsIncompatibility,msg:" des options sont incompatibles"} ))
            dispatch(setRetrieveConfigurationStatus('failed'))
         } 
        if (OptionsAreMissing) {
            dispatch(setRetrieveConfOptionsNotInList({list: optionsMissing, msg:"des options sont manquantes"}))
            dispatch(setRetrieveConfigurationStatus('failed'))
        } 

    }, [])

   
    return (
            <div>
                {/* show errors (incompatibilities and missing options) */}
                {/* <div>
                    { optionsMissing && optionsMissing.length > 0 && 
                        <div>options not found:{(optionsMissing as string[]).join(' ')} </div>
                    }
                    { optionsIncompatibility && optionsIncompatibility.length > 0  &&  
                        <div>options Incompatibles: {(optionsIncompatibility as string[]).join(' ')} </div>
                    }
                </div> */}
                {/* no error process next step and add option */}
                { optionsIncompatibility && optionsIncompatibility.length <=  0 &&  optionsMissing && optionsMissing.length <= 0 &&
                    <PopulateOptions
                        configData={configData}
                        configurationOptionsList={configurationOptionsList}
                        modelOptionsList={modeloptionsList}
                    />
                }
            </div>
    )
}

export default TestOptions