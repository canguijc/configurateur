import { useEffect } from "react"
import LoadingSpinner from "../../../../components/ui/spinner/LoadingSpinner"
import { useGetAllModelApiQuery } from "../../../../store/features/api/apiSlice"
import { clearEngineSettings, setModel } from "../../../../store/features/engineSetting/engineSettingSlice"
import { setRetrieveConfigurationStatus, setRetrieveError } from "../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch } from "../../../../store/hooks/reduxHooks"

export interface RetrieveModelProps {
    modelRef: string
    gammeRef: string
}
const RetrieveModel = ({gammeRef, modelRef}: RetrieveModelProps) => {
    const dispatch = useAppDispatch()
    let content: any

    const model = useGetAllModelApiQuery(gammeRef, {
        selectFromResult: ({data, isLoading, isError, isSuccess, isUninitialized}) => ({
            data: data,
            isUninitialized,
            isError,
            isLoading,
            isSuccess,
            findModel: (ref: string) => {
                if (data) {
                    return data.models.find(model => model.ref === ref)
                }
            }
        })
    })

    useEffect(() => {
        if (model.isSuccess) {
            const modelFound = model.findModel(modelRef)
            if (modelFound) {
               dispatch(setModel(modelFound)) 
               dispatch(setRetrieveConfigurationStatus('canCheckOptions'))
            }
            else {
                //  error 
                dispatch(clearEngineSettings())
                dispatch(setRetrieveError('le modèle n\' est pas disponible dans cette gamme'))
                dispatch(setRetrieveConfigurationStatus('failed'))
            }
        }
    }, [model])

    if (model.isError) {
        dispatch(setRetrieveError('erreur lors du chargement du modèle'))
        dispatch(setRetrieveConfigurationStatus('failed'))
        content= <div>error loading models</div>
    } else if (model.isLoading) {
        content =(
            <div>
                <div>
                    chargement des modèles...
                    <LoadingSpinner/>
                </div>
            </div>
        ) 
    }
    return (
        <div> 
            {content}
        </div>
    )
}

export default RetrieveModel