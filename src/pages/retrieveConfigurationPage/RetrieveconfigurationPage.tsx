import { useEffect } from 'react'
import { clearEngineSettings, selectOptionStatusNecessity } from '../../store/features/engineSetting/engineSettingSlice'
import { clearRetrieveStatus, selectRetrieveConfigurationData, selectRetrieveStatus, setRetrieveConfigurationStatus } from '../../store/features/retrieveConfiguration/retrieveConfigurationSlice'
import { useAppDispatch, useAppSelector } from '../../store/hooks/reduxHooks'
import RetrieveOptions from './components/retrieveOptions/RetrieveOptions'
import RetrieveModel from './components/retrieveModel/RetrieveModel'
import { Navigate, useNavigate } from 'react-router-dom'
import appConfig from '../../appconfig'
import RetrieveGamme from './components/retrieveGamme/RetrieveGamme'
import useAppClearStore from '../../store/hooks/useAppClearStore'
import { showPopup } from '../../store/features/modalpopup/modalPopupSlice'
import { ModalConfirmationDefaultId } from '../../components/confirmationModal/ConfirmationModal'
import { clearSaveConfiguration } from '../../store/features/saveConfiguration/saveConfigurationSlice'

// const config = {
// 		id_save_conf: "5",
// 		id_user: "9",
// 		name: "rere",
// }
// const data = '{"gamme":{"label":"Compact","ref":"COM"},"model":{"label":"TF30.9G-75","ref":"MA00878"},"options":["V2511","V0216","V2500","V1510","V1022","V2200","V1320","V1413","V8179","V8180","V9179","A0271","A0718","A0720","A0894","A0895","A2331","A2332","VTBL360"],"total":{"totalOptionsPrice":41701,"totalConfigurationPrice":159938}}'

/* flow to retrieve  a saved configuration
    1. clear engineSettings
    2. check if gamme is valid
    3. add gamme in engineSetting
    4. check if model is found
    5. add model in engineSetting
    6. check if all options in saved configuration array  are present in options Model
    7. check if no incompatibility error 
    8. add all options in engineSetting
    9. check if no necessity  error? maybe not needed
    10. redirect to options page
*/
const RetrieveConfigurationPage = () => {
    const dispatch = useAppDispatch()
    const retrieveStatus = useAppSelector(selectRetrieveStatus)
    const savedConfiguration = useAppSelector(selectRetrieveConfigurationData)
    const optionsNecessities = useAppSelector(selectOptionStatusNecessity)
    const clearAppStore = useAppClearStore()
    const navigate = useNavigate()
    const optionsPagePath = appConfig.pages.options.link.abs
    const resumePagePath = appConfig.pages.resume.link.abs
    const gammesPagePath = appConfig.pages.gammes.link.abs

    const modalRetrieveSuccessId: ModalConfirmationDefaultId = "modalRetrieveConfigurationSuccess"
    const modalRetrieveErrorId: ModalConfirmationDefaultId = "modalRetrieveConfigurationError"

    //clear engineSettings and the current configuration on first render
    useEffect(() => {
        if (savedConfiguration && retrieveStatus === "initRetrieving" ) {
            dispatch(clearEngineSettings())
            dispatch(clearRetrieveStatus())
            dispatch(clearSaveConfiguration())
            dispatch(setRetrieveConfigurationStatus("canCheckGamme"))
        } else {
            clearAppStore()
            navigate(gammesPagePath)
        }
    }, [])

    // show the different popup
    useEffect(() => {
        if ( retrieveStatus === "succeeded") {
            if (optionsNecessities.length <= 0 ) {
                // no necessities go to resume Page
                navigate(resumePagePath)
            } else {
                // else  go to options Page to reconfigure
                navigate(optionsPagePath)
            }
            // show confirmation modal
            dispatch(showPopup(modalRetrieveSuccessId))
        }
        if ( retrieveStatus === "failed") {
            // show error modal
            dispatch(showPopup(modalRetrieveErrorId))
        }
    },[retrieveStatus, optionsNecessities])


    return (
        <div>
            { savedConfiguration &&  retrieveStatus == "canCheckGamme"  && 
                <RetrieveGamme
                    gammeRef={savedConfiguration.conf.gamme.ref}
                />
            }
            { savedConfiguration &&  retrieveStatus === "canCheckModel" && 
                <RetrieveModel
                    gammeRef={savedConfiguration.conf.gamme.ref}
                    modelRef={savedConfiguration.conf.model.ref}
                />
            }
            { savedConfiguration &&  retrieveStatus === "canCheckOptions" &&
                <RetrieveOptions
                    modelRef={savedConfiguration.conf.model.ref}
                    optionsListRef={savedConfiguration.conf.options}
                    configData ={{id_save_conf:savedConfiguration.id_save_conf, id_user: savedConfiguration.id_user, name: savedConfiguration.name, dateCreAt: savedConfiguration.dateCreAt, agent: savedConfiguration.agent, nom: savedConfiguration.nom, prenom: savedConfiguration.prenom}}
                /> 
            }
            { retrieveStatus === "succeeded" && 
                <Navigate to={optionsPagePath}/>
            }
        </div>
    )
}
export default RetrieveConfigurationPage