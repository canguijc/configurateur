import { t } from "i18next"
import { useTranslation } from "react-i18next"
import { Link } from "react-router-dom"

const NoMatchPage = () => {
    const {t} = useTranslation()
    return (
        <div className='mx-2'>
            <p>{t("cfg_no_page_msg")}</p>
            <Link to={'/'}>{t("cfg_ret_accueil")}</Link>
        </div>
    )
}

export default NoMatchPage