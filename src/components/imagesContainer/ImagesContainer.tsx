import appConfig from "../../appconfig"

export interface ImagesContainerProps {
    images: string[]
}

const ImagesContainer = ({images}: ImagesContainerProps) => {
    const imagesPath = appConfig.url.imagesDirUrl 
    return (
        <div className="w-100 d-flex justify-content-center my-2 py-2">
            { images && images.map((image, index) => 
                    <img 
                        key={index}
                        className="img-fluid"
                        src={`${imagesPath}/${image}`} alt={`img-${index}`} 
                    />
                )
            }
        </div>
    )
}

export default ImagesContainer