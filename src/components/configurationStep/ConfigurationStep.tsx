import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import appConfig, { AppPageIndex } from '../../appconfig'

// style 
import './configurationStep.scss'

const ConfigurationStep = () => {
    const pathname = useLocation().pathname
    const {t} = useTranslation()
    const appPages = appConfig.pages 

    const currentPath = useMemo(() => {
        const key : AppPageIndex | undefined = (Object.keys(appPages).find(el  => appPages[el as AppPageIndex ].link.abs == pathname)) as AppPageIndex
        if (key) {
            return appPages[key] 
        }
    }, [pathname, appPages])

    if (currentPath)  {
        return (
            <div className="row mt-2 mb-3 mx-0"> 
                {currentPath.step && currentPath.title && 
                    <h2 className="config-step-title text-center">{`${currentPath.step + '/ -'} ${ t(currentPath.title)} `}</h2>
                }
            </div>
        )
    } else {
        return null
    }
}

export default ConfigurationStep