import { useNavigate } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { useAppDispatch, useAppSelector } from '../../store/hooks/reduxHooks'
import { Gamme } from '../../store/features/gamme/gammesData'

import BtnValid from '../ui/button/customBtn/CustomBtn'
import { clearEngineSettings, setGamme } from '../../store/features/engineSetting/engineSettingSlice'

// style
import './gammeCard.scss'
import appConfig from '../../appconfig'

interface GammeCardProps {
     gamme: Gamme
}
const GammeCard = ({gamme}: GammeCardProps) => {
    const {t} = useTranslation()
    // hook to navigate with react-router-dom
    let navigate = useNavigate()
    // dispatch to redux store
    const dispatch = useAppDispatch()
    const modelsPage = appConfig.pages.models.link.abs
    // access model state  in store
    const {error: gammeError} = useAppSelector(store => store.gamme) 

    const handleClick = () => {
        // dispatch  the selected gamme 
            // fetch the models corresponding to the selected gamme 
            // to remove  if engineSettingSlice and apislice are working
            // using engineSettingSlice 
            dispatch(clearEngineSettings())
            dispatch(setGamme(gamme))
            navigate(modelsPage,{state:{currentGamme:gamme}})
        }

    return (
        <div className='d-flex flex-column align-items-center mx-auto mw-100'>
            <div className='product-card text-center d-inline-flex  pb-3 m-1 mb-3'>
                    <img src={gamme.image} className="img-fluid"  alt={gamme.alt} />   
            </div >
            <h4 className="product-card-model-name">{gamme.gamme}</h4>
            <BtnValid
                label={t('cfg_choi_btn').toUpperCase()}
                onClick={handleClick}
            />
        </div>
    )
}

export default GammeCard