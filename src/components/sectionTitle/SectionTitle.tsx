import React from 'react'
import './sectionTitle.scss'

interface SectionTitleProps  {
    title: string,
    align?: "left" | "center" | "right"
    border?: boolean
    children?: React.ReactNode 
}

const SectionTitle  = ({title, children , border=true, align="center"}: SectionTitleProps) => {
    return (
        <div className={` mb-2 ${ border && "section-header pb-2"}`}>
            <h4 className={`section-title text-${align}`}>{title} {children} </h4>
        </div>
    )
}

export default SectionTitle