import logo from '../../assets/m_merlo-fond-noir.svg'
import { useTranslation } from "react-i18next";
import SelectLanguage from '../selectLanguage/SelectLanguage';

import './navbar.scss'
import { getUserNameInSessionStoragehelper } from '../../helpers/sessionStoragehelpers';
import { getUserProfil } from '../../helpers/sessionStoragehelpers';
import { getUserRole } from '../../helpers/sessionStoragehelpers';
import { Link } from 'react-router-dom';
import { RetrieveConfigurationLink } from '../ui/link/CustomLink/customLink';
import { AdminConfigurationLink } from '../ui/link/CustomLink/customLink';
import { useAppDispatch } from '../../store/hooks/reduxHooks';
import { showPopup } from '../../store/features/modalpopup/modalPopupSlice';
import CustomModal from '../ui/modal/customModal/CustomModal';
import RetrieveConfigurationList from '../ui/modal/_components/retrieveConfigurationList/RetrieveConfigurationList';
import { useGetUserJwtMutation } from "../../store/features/api/apiSlice"
import {useEffect, useState} from 'react';

// const type=getUserProfil();
// const role=getUserRole();


// if(role=="ROLE_RESPONSABLE_VENTES" || role=="ROLE_ADMIN" || (role=="ROLE_AGENT" && type=='1')){
//     classPlus="";
// }


// async function fetchData() {
//     const data1 = await  getUserProfil().then( async r=>{
//             //console.log(r.data)
//             // @ts-ignore
//         //setType(r)
//             return r
//         }
//     );
//     const data2 = await  getUserRole().then(async r=>{
//         // @ts-ignore
//         //setRole(r)
//         return r
//     });
//     return Promise.all([data1, data2]).then(([data1, data2]) => {
//         console.log(data1,data2)
//         // setAllData(categorizeFiches(data2,data1));
//
//     });
// }
//
// useEffect(() => {
//
//     fetchData().then(async r=>{
//        // setActive(true)
//     })
//
//     //setAllData(categorizeFiches(categories,fiches));
//
// }, []);


const lien2=() => {
    const profil=getUserProfil()
    if(profil && profil.toString()=='5') return 'lien';
    else return ''
}
interface IPost {
        active: boolean
}
const Navbar = ( post : IPost) => {
    const {t, i18n} = useTranslation()
    const dispatch = useAppDispatch()
    const username = getUserNameInSessionStoragehelper()
    console.log("name",username)
    const savedConfigModalId = "savedConfigModalId"
    const handleShowRetrieveConfig = () => {
        console.log('click')
        dispatch(showPopup(savedConfigModalId))
    }
    console.log("navbar");
    console.log(post.active);
    return (
        <div className="nav-main-navbar d-flex flex-column">
            <CustomModal 
                id={savedConfigModalId}  
                title={t('cfg_mod_config_sauv')} 
                actionsHandledInChildren={true}
            >
                {/*TODO: implement the popup  */}
                <RetrieveConfigurationList
                />
            </CustomModal>
          <div className="row pt-2 pb-1 mx-0">
          </div>
          <div className="row  mx-0">
            {/* logo */}
            <div className="row mx-0">
              <div className="col-4">
                <img className="img-logo" src={logo} alt="logo merlo" />
              </div>
                <div className="col-4 d-flex align-items-center">
                  <h2 className="config-title text-center  m-auto" >{t('cfg_configurateur')}</h2>
                </div>
              <div className="col-4 d-flex flex-row justify-content-end align-items-center ">
                <div >
                  <h5 className="nav-store-location-link mx-1 mx-sm-auto px-2">{ username ? username : `B. Vandeme`}</h5>
                </div>
                <div className="mx-1">
                  <SelectLanguage/>
                </div>
              </div>
              
            </div>
          </div>
          <div 
            className="row d-flex flex-column flex-grow-1 justify-content-end mb-2 mx-0"

            >
            <div className="d-flex justify-content-center pb-1 ">
              <RetrieveConfigurationLink 
                  text={t('cfg_ret_conf')}  onClick={handleShowRetrieveConfig}
              />

                { post.active &&
                <AdminConfigurationLink text={t('cfg_adm_conf')} />
                }

              </div>
          </div>
        </div>
    );
  }



export default Navbar