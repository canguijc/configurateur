import { ChangeEventHandler } from "react"
import config, { Language} from "../../appconfig"
import { changeLanguage, selectCurrentLanguage } from "../../store/features/translate/translateSlice"
import { useAppDispatch, useAppSelector } from "../../store/hooks/reduxHooks"

// styles
import './selectLanguage.scss'

const SelectLanguage = () => {
    // contains the defined languages to use
    const langArray = config.translation.languages 
    // access store and get the current language
    const currentLang = useAppSelector(selectCurrentLanguage)
    const dispatch = useAppDispatch()
    console.log('langue',currentLang)
  const handleSelectLanguage: ChangeEventHandler<HTMLSelectElement> = (e) => {
    // need to be of Language type 'fr' 'de' etc...
    const selectedLang = e.target.value as Language  
    // update the store state for the selected language
    dispatch(changeLanguage(selectedLang))
  }
    return (
        <select 
            className="select-lang"
            value={currentLang}
            onChange={handleSelectLanguage}
        >
            {
                langArray.map(language =>
                        <option className="option-lang" key={language} value={language}>{language.toUpperCase()}</option>
                )
            }
        </select>
    )
}

export default SelectLanguage