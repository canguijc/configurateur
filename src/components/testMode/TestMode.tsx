import { useTranslation } from "react-i18next"
import { selectTestMode } from "../../store/features/engineSetting/engineSettingSlice"
import { useAppSelector } from "../../store/hooks/reduxHooks"

const TestMode = () => {
    const testMode = useAppSelector(selectTestMode)
    const {t} = useTranslation()

    if (testMode) {
        return (
            <div className="d-flex px-2 py-2 text-danger fw-bold justify-content-center">
               <span>{t('cfg_mod_temp')}</span> 
            </div>
        )
    } 
    return null
}

export default TestMode