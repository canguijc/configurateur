import { MouseEvent, useState } from "react"
import { useTranslation } from "react-i18next"


import { useAppDispatch, useAppSelector } from "../../../../../../store/hooks/reduxHooks"
import { OptionRowTransformed } from "../../../../../../store/features/api/apiSlice"
import { addOrRemoveOption, NecessityOptionStatus, selectOptionsRef, selectOptionStatusIncompatibility } from "../../../../../../store/features/engineSetting/engineSettingSlice"
import { selectCurrentLanguage } from "../../../../../../store/features/translate/translateSlice"

import { showPopup } from "../../../../../../store/features/modalpopup/modalPopupSlice"
import CustomModal from "../../../../modal/customModal/CustomModal"
import { InCompatibilityLink, InfoLink } from "../../../../link/CustomLink/customLink"

import { formatPrice } from "../../../../../../utils/priceUtils"

// style
import './optionRow.scss'
import OptionInformation from "../../../../modal/_components/optionInformation/OptionInformation"
import CleanDiv from "../../../../div/cleanDiv/CleanDiv"
 


export interface OptionRowProps {
    option: OptionRowTransformed
    modelRef: string
}

const OptionRow = ({option}: OptionRowProps) => {
    const {t} = useTranslation()
    const optionsSelected = useAppSelector(selectOptionsRef)
    const currentLang = useAppSelector(selectCurrentLanguage)
    const optionsStatusIncompatibility = useAppSelector(selectOptionStatusIncompatibility)
    const dispatch = useAppDispatch()
    const optionId = `option-${option.ref}`
    const infoModalId = `info-${optionId}`
    const incompatibilityModalId = `compatibility-${optionId}`

    const handleInfoLink = (event: MouseEvent<HTMLDivElement>) => {
        dispatch(showPopup(infoModalId))
    }
    const handleCompatibilityLink = (event: MouseEvent<HTMLDivElement>) => {
        dispatch(showPopup(incompatibilityModalId))
    }
    const handleClickOption = () => {
        // if incompatibilities is null no incompatibility or necessity for the selected option
        dispatch(addOrRemoveOption(option))
    }

    return (
        <tr
          className={`${optionsStatusIncompatibility.includes(option.ref) && 'option-incompatibility'}`}
        >
            {/* display option info */}
            <CustomModal id={infoModalId} title={t("cfg_mod_info")}>
                <OptionInformation option={option}/>
            </CustomModal>

            {/* display incompatibility */}
            <CustomModal id={incompatibilityModalId} title={t("cfg_mod_info")}>
                <CleanDiv 
                    className="font-bold mb-1"
                    dangerousHTML={ option[currentLang].label }/>
                <CleanDiv
                    className="font-regular"
                    dangerousHTML={option[currentLang].alert}
                />
            </CustomModal>

            <th scope="row">
                <div className="d-block">
                    <input 
                        type="checkbox" 
                        name={`option-${option.ref}`} 
                        id={`option-${option.ref}`} 
                        value={`option-${option.ref}`} 
                        checked={optionsSelected.includes(option.ref)}
                        disabled={optionsStatusIncompatibility.includes(option.ref) }
                        onChange={handleClickOption}
                    />
                </div>
            </th>
            <td>
            <div className="row">
                {/* option label */}
                <CleanDiv
                    className={`option-name col-9 ${!optionsStatusIncompatibility.includes(option.ref) ? "option-hover" : "option-label-disable" }`}
                    dangerousHTML={option[currentLang].label}
                    onClick={!optionsStatusIncompatibility.includes(option.ref) ? handleClickOption : undefined}
                />
                {/* product ref */}
                <div className="option-ref col-3 text-end">
                    <span>{option.ref}</span>
                </div>
            </div>
            <div className="row mt-3 mb-2"> 
                {/* information and alerts links */}
                <div className="col-9 d-flex">
                    { option[currentLang].comment && 
                        <InfoLink
                            text={t("cfg_plus_info")}
                            onClick={handleInfoLink}
                        /> 
                    }
                    { option[currentLang].alert && 
                        <InCompatibilityLink
                            text={t('cfg_compat')}
                            onClick={handleCompatibilityLink}
                        />
                    }
                </div>
                <div className="option-price col-3 text-end">
                    {`${formatPrice(option.prix)}`} 
                </div>
            </div>
            </td>
        </tr>
    ) 
}

export default OptionRow