import { MouseEvent, useState, useEffect } from "react"
import { useTranslation } from "react-i18next"


import { useAppDispatch, useAppSelector } from "../../../../../../store/hooks/reduxHooks"
import { NecessityConstraint, OptionRowTransformed } from "../../../../../../store/features/api/apiSlice"
import { addOrRemoveOption, NecessityOptionStatus, selectOptionsRef, selectOptionStatusIncompatibility } from "../../../../../../store/features/engineSetting/engineSettingSlice"
import { selectCurrentLanguage } from "../../../../../../store/features/translate/translateSlice"

import { showPopup } from "../../../../../../store/features/modalpopup/modalPopupSlice"
import CustomModal from "../../../../modal/customModal/CustomModal"
import { InCompatibilityLink, InfoLink } from "../../../../link/CustomLink/customLink"

import { formatPrice } from "../../../../../../utils/priceUtils"
import { DangerPicto } from "../../../../picto/Picto";
import { arrayIntersectUtils } from "../../../../../../utils/arrayUtils"

// style
import './optionRow.scss'
import OptionInformation from "../../../../modal/_components/optionInformation/OptionInformation"
import CleanDiv from "../../../../div/cleanDiv/CleanDiv"
 


export interface OptionRowDevModeProps {
    option: OptionRowTransformed
    modelRef: string
}

/**
 *  row of OptionListTable 
 */

// disable is not use 
// const necessityStatusInitialState = {necessityN: [], otherNecessity: {}, disable: false}
const necessityStatusInitialState = {necessityN: [], otherNecessity: {}, ref:'noNeed', count:0}

const OptionRowDevMode = ({option, modelRef}: OptionRowDevModeProps) => {
    const [necessityStatus, setNecessityStatus] = useState<NecessityOptionStatus>(necessityStatusInitialState)
    const [showOtherNecessityInfo, setShowOtherNecessityInfo] = useState(false)
    const [showNecessityInfo, setShowNecessityInfo] = useState(false)
    const {t} = useTranslation()
    const optionsSelected = useAppSelector(selectOptionsRef)
    const currentLang = useAppSelector(selectCurrentLanguage)
    const optionsStatusIncompatibility = useAppSelector(selectOptionStatusIncompatibility)
    const dispatch = useAppDispatch()
    const optionId = `option-${option.ref}`
    const infoModalId = `info-${optionId}`
    const incompatibilityModalId = `compatibility-${optionId}`

    useEffect(() => {
        if(option.constraint?.necessite && option.constraint.necessite){
            // copy of showNecessityInfo state
            const necessityKeys = Object.keys(option.constraint.necessite).length
            let necessityStatusCopy = {...necessityStatus}
            let otherNecessityNotFullFiled: NecessityConstraint = {} 
            if (necessityKeys > 0){
                // necessity N
                const necessityNKey: any = "N"
                const hasNecessityN = option.constraint.necessite.hasOwnProperty(necessityNKey)
                if (hasNecessityN){
                    const necessityN = option.constraint.necessite[necessityNKey] as unknown as string[]
                    const isNecessityNFilled = arrayIntersectUtils(necessityN, optionsSelected).length > 0
                    if(!isNecessityNFilled) {
                        // fill the necessity N needed
                        necessityStatusCopy.necessityN = necessityN
                        //  change it to true to  disable the checkbox when N is not fullfilled
                        // necessityStatusCopy.disable = false
                    } else {
                        // remove the necessity N needed
                        // necessity N of option fullfilled enable option 
                        necessityStatusCopy.necessityN = []
                        // necessityStatusCopy.disable = false 
                        // disable button when necessity  N is needed
                        // necessityStatusCopy.disable = false
                    } 
                } 

                // other necessity N1 N2 N3 ...
                Object.keys(option!.constraint!.necessite!).forEach((necessityKey) => {
                    // skip this iteration
                    if (necessityKey == necessityNKey) return ;
                    // process other necessities
                    const isOtherNecessityNFullfilled = arrayIntersectUtils((option!.constraint!.necessite![necessityKey] as unknown as string[]), optionsSelected).length > 0
                    if (!isOtherNecessityNFullfilled) {
                        otherNecessityNotFullFiled[necessityKey] = option!.constraint!.necessite![necessityKey] 
                    } else {

                    }
                })

                necessityStatusCopy.otherNecessity = otherNecessityNotFullFiled 
                setNecessityStatus(necessityStatusCopy)
               
            } 
        }
    },[optionsSelected])

    const handleInfoLink = (event: MouseEvent<HTMLDivElement>) => {
        dispatch(showPopup(infoModalId))
    }
    const handleCompatibilityLink = (event: MouseEvent<HTMLDivElement>) => {
        dispatch(showPopup(incompatibilityModalId))
    }

    const handleClickOption = () => {
        // if incompatibilities is null no incompatibility or necessity for the selected option
        dispatch(addOrRemoveOption(option))
    }

    const handleOtherNecessityInfo = () => {
        setShowOtherNecessityInfo(prevState => !prevState)
    }
    const handleNecessityInfo = () => {
        setShowNecessityInfo(prevState => !prevState)
    }

    return (
        <tr
          className={`${optionsStatusIncompatibility.includes(option.ref) && 'option-incompatibility'}`}
        >
            {/* display option info */}
            <CustomModal id={infoModalId} title={t("cfg_mod_info")}>
                <OptionInformation option={option}/>
            </CustomModal>

            {/* display incompatibility */}
            <CustomModal id={incompatibilityModalId} title={t("cfg_mod_info")}>
                <CleanDiv 
                    className="font-bold mb-1"
                    dangerousHTML={option[currentLang].label}
                />
                {/* TODO:  change the  rendering of  string without escaping as it is really dangerous */}
                <CleanDiv 
                    className="font-regular"
                    dangerousHTML={option[currentLang].alert}
                />
            </CustomModal>

            <th scope="row">
                <div className="d-block">
                    <input 
                        type="checkbox" 
                        name={`option-${option.ref}`} 
                        id={`option-${option.ref}`} 
                        value={`option-${option.ref}`} 
                        checked={optionsSelected.includes(option.ref)}
                        disabled={optionsStatusIncompatibility.includes(option.ref) }
                        onChange={handleClickOption}
                    />
                    {/*show necessity N if not fullfilled   */}
                    <div className="mt-2">
                        { necessityStatus.necessityN  && necessityStatus.necessityN.length > 0 && 
                            <DangerPicto
                                onClick={handleNecessityInfo}
                            />
                        }
                    </div>

                    {/*show other necessity if not fullfilled   */}
                    <div className="mt-2">
                        { necessityStatus.otherNecessity  && Object.keys(necessityStatus.otherNecessity).length > 0 && 
                            <DangerPicto
                                onClick={handleOtherNecessityInfo}
                            />
                        }
                    </div>

                </div>
            </th>
            <td>

                {/*  necessity N info */}
                { showNecessityInfo  && ( necessityStatus.necessityN  && necessityStatus.necessityN.length > 0) && 
                    <div className="row option-necessity-info"> 
                        { necessityStatus && necessityStatus.necessityN &&    
                            <div 
                            >
                                N:{necessityStatus.necessityN.join(' ')}
                            </div> 
                        }
                    </div>
                }

                {/* other necessity info */}
                { showOtherNecessityInfo && 
                    <div className="row option-other-necessity-info"> 
                        { necessityStatus && necessityStatus.otherNecessity &&   Object.keys(necessityStatus.otherNecessity).map( (keyNecessity: string) => 
                            <div 
                                key={keyNecessity}
                            >
                                {keyNecessity}:{option!.constraint!.necessite![(keyNecessity as  any)].toString() }
                            </div> 
                        )
                        }
                    </div>
                }
                <div className="row">
                    {/* option label */}
                    <CleanDiv 
                        className={`option-name col-9 ${!optionsStatusIncompatibility.includes(option.ref) ? "option-hover" : "option-label-disable" }`}
                        dangerousHTML={option[currentLang].label}
                        onClick={!optionsStatusIncompatibility.includes(option.ref) ? handleClickOption : undefined}
                    />
                    {/* product ref */}
                    <div className="option-ref col-3 text-end">
                        <span>{option.ref}</span>
                    </div>
                </div>
                <div className="row mt-3 mb-2"> 
                    {/* information and alerts links */}
                    <div className="col-9 d-flex">
                        { option[currentLang].comment && 
                            <InfoLink
                                text={t("cfg_plus_info")}
                                onClick={handleInfoLink}
                            /> 
                        }
                        { option[currentLang].alert && 
                            <InCompatibilityLink
                                text={t('cfg_compat')}
                                onClick={handleCompatibilityLink}
                            />
                        }
                    </div>
                    <div className="option-price col-3 text-end">
                        {`${formatPrice(option.prix)}`} 
                    </div>
                </div>
            </td>
        </tr>
    ) 
}

export default OptionRowDevMode