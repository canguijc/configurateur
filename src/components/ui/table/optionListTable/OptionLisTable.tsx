import { OptionRowTransformed } from '../../../../store/features/api/apiSlice'
import OptionRow from './components/optionRow/optionRow'
import './optionListTable.scss'

type OptionListTableProps  =  {
    options: OptionRowTransformed[] | undefined
    modelRef: string
}

const OptionListTable = ({options, modelRef}: OptionListTableProps) => {
    return (
        <table className="table">
            <tbody>
                { options &&  options.map(option => 
                    <OptionRow
                        key={option.ref}
                        option={option}
                        modelRef={modelRef}
                    />
                )
                }
            </tbody>
        </table>
    ) 
}

export default OptionListTable