import {  useTranslation } from 'react-i18next'
import { Model } from '../../../../store/features/api/apiSlice'
import { useAppDispatch, useAppSelector } from '../../../../store/hooks/reduxHooks'
import ModelListRow from './components/ModelListRow'

import './modelListTable.scss'

interface  ModelLisTableProps  {
    models: Model[]

}
const ModelListTable = ({models}: ModelLisTableProps) => {
    // const [selectedRow, setSelectedRow] = useState<string|null>(null)
    const {t} = useTranslation()
    // const {data, loading} = useAppSelector(state => state.model)

    // put the selected model in store.model.currentModel 
    // comment this if check radio on row click

    /**
     * this version check the radio button when clicking on row 
     * uncomment to get checked box when row click 
     */
    // const handleSelectRow = ( e:any ) => {
    //     const refModel =  e.currentTarget.id
    //     setSelectedRow(refModel)
    //     dispatch(selectModel(refModel))
    // }

    return (
            <table className=" model-list-table table table-hover">
                <tbody>
                    {/*  show each model of a gamme */}
                    { models.map(model => 
                        <ModelListRow
                            key={model.ref}
                            model={model} 
                        />
                        )
                    }
                </tbody>
            </table>
        
    ) 
}

export default ModelListTable