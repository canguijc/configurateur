import { ChangeEvent, ChangeEventHandler } from "react"
import appConfig from "../../../../../appconfig"
import { showPopup } from "../../../../../store/features/modalpopup/modalPopupSlice"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import ModelSpecList from "../../../modal/_components/modelSpecList/ModelSpecList"
import { calcPriceTTC, formatPrice } from "../../../../../utils/priceUtils"
import { InfoLink } from "../../../link/CustomLink/customLink"
import CustomModal from "../../../modal/customModal/CustomModal"

//style
import '../modelListTable.scss'
import { clearOptions, selectModel, setModel } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { Model } from "../../../../../store/features/api/apiSlice"
import { t } from "i18next"

export interface ModelListRowProps {
    model: Model
}

const ModelListRow = ({model}: ModelListRowProps) => {

    const {defaultVAT, defaultCurrency} = appConfig.app
    const infoModelId = `info-model-${model.ref}`
    const currentModel = useAppSelector(selectModel)

    const dispatch = useAppDispatch() 
    // put the selected model in store.model.currentModel 
    // comment this if check radio on row click
    const handleSelectModel: ChangeEventHandler<HTMLInputElement> = ( event: ChangeEvent<HTMLInputElement> ) => {
        dispatch(setModel(model))
        dispatch(clearOptions())
    }

    const handleClickInfo = () => {
        dispatch(showPopup(infoModelId))
    }

    return (
        <tr 
            id={model.ref}
        >
            <CustomModal 
                id={infoModelId} 
                title={t('cfg_mod_info').toUpperCase() }
                size={'lg'}
            >
                <ModelSpecList 
                    modelLabel={model.label}
                />
            </CustomModal>
            <th scope="row">
                <label className="model-hover text-nowrap">
                    <input className="model-ref" 
                        type="radio" 
                        value={model.ref} 
                        name="model"
                        checked={model.ref == currentModel?.ref ? true : false }
                        // comment this to check radio when clicking a row
                        onChange={handleSelectModel}
                    />

                        <div className="d-inline-block model-label px-1 text-nowrap">
                            <span className="px-1">{model.label}</span> 
                            <InfoLink
                                onClick={handleClickInfo}
                            />
                        </div>
                </label>
            </th>
            <td className="model-currency">{defaultCurrency.toUpperCase()}</td>
            <td className="text-center">
                    <span className="model-price-ht d-block d-lg-inline text-nowrap mx-2">{`${formatPrice(model.prix)} ${t('cfg_price_ht').toUpperCase()}` }</span> 
                    <span className="model-price-ttc d-block d-lg-inline text-nowrap" >{`${formatPrice(calcPriceTTC(model.prix, defaultVAT))} (${t('cfg_price_ttc').toUpperCase()})`}</span>
            </td>
        </tr>
    )
}

export default ModelListRow