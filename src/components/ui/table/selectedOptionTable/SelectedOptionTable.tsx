import appConfig from '../../../../appconfig'
import { selectModel, selectOptions, selectTotalConfigurationPrice, selectTotalOptionsPrice } from '../../../../store/features/engineSetting/engineSettingSlice'
import { useAppDispatch, useAppSelector } from '../../../../store/hooks/reduxHooks'
import { calcPriceTTC, formatPrice } from '../../../../utils/priceUtils'
import SelectedOptionsContainer from './components/SelectedOptionsContainer'
import SelectedModelRow from './components/SelectedModelRow'
import { useTranslation } from 'react-i18next'
import CustomModal from '../../modal/customModal/CustomModal'
import { showPopup } from '../../../../store/features/modalpopup/modalPopupSlice'
import FinancingModal from '../../modal/_components/financingModal/FinancingModal'

import './selectedOptionTable.scss'
import { selectFinancing } from '../../../../store/features/financing/financingSlice'
import { useCallback } from 'react'
import { calcMensuality } from '../../../../helpers/financingHelpers'

type ModelStyle = {
        border: 'border-top-bottom' | 'border-top-thick' | 'border-bottom-thick'
}
export interface SelectedOptionTableProps {
    modelStyle?: ModelStyle 
    modelImageModalId?: string 
}

const modelStyleDefault: ModelStyle = {
    border: 'border-top-bottom'
}


const SelectedOptionTable = ({modelStyle=modelStyleDefault, modelImageModalId}: SelectedOptionTableProps) => {
    const {t} = useTranslation()
    const currentModel= useAppSelector(selectModel)
    const options = useAppSelector(selectOptions) 
    const totalPrice = useAppSelector(selectTotalConfigurationPrice)
    const financing = useAppSelector(selectFinancing)
    const dispatch = useAppDispatch()
    const vat = appConfig.app.defaultVAT
    const currency = appConfig.app.defaultCurrency
    const financingModalId = 'financingModalIdInSelectedOptionTable'

    // calculate the mensuality from financingSlice and engineSettingSlice totalprice
    const calcMensualityCallback = useCallback(() =>{
        const priceTTC = calcPriceTTC(totalPrice, vat)
        const monthlyPay = calcMensuality(priceTTC, financing.durationMonth ,financing.rate,financing.apport,financing.echeance)
            return  monthlyPay && isFinite(monthlyPay) ? monthlyPay : undefined
    }, [financing, totalPrice])

    const handleClickFinancing  = () => {
        dispatch(showPopup(financingModalId))
    }

/*
    let labEcheance="";
    if(financing.echeance==1) labEcheance="mois";
    if(financing.echeance==3) labEcheance="trimestre";
    if(financing.echeance==6) labEcheance="semestre";
    if(financing.echeance==12) labEcheance="an";
*/
    let labEcheance=t("cfg_fin_echeance_"+financing.echeance);

    // no selected model 
    if (!currentModel) {
        return null 
    }

    // get the selected model 
    return (
        <div>
            <CustomModal 
                //title= {t('cfg_finance')}
                title= {t('cfg_finance')}
                id={financingModalId}
                actionsHandledInChildren={true}
            >
                <FinancingModal/>
            </CustomModal >

            {/* model */}
            <div className={`${modelStyle.border} mt-3 mb-2`}>
                {/* 
                    TODO: add color background transition when an option is added 
                    https://stackoverflow.com/questions/45686557/how-to-make-backgrounds-color-transition-on-a-react-component
                */}
                <SelectedModelRow
                    label={currentModel?.label}
                    price={currentModel ? formatPrice(currentModel.prix) : "Nan"}
                    modelImageModalId={modelImageModalId}
                />
            </div>

            {/* options container */}
            {  options.length > 0 && 
                <div className="border-bottom-thick">
                    <div className="mt-3 mb-1">
                      <SelectedOptionsContainer
                        modelRef={currentModel.ref}                       
                      />  
                    </div>
                </div>
            }
            
            {/* total price HT adn TTC */}
            <div className="my-1">
                <div className="price-bold d-flex justify-content-end">{formatPrice(totalPrice)}</div>
                <div className=" d-flex justify-content-end">
                    <span className="price-bold px-1">{`${t('cfg_price_ttc').toUpperCase()} :`}</span>
                    <div
                        className="finance-wrapper"
                        onClick={handleClickFinancing} 
                    >
                        <span className="price-mensuality px-1"><u>{ calcMensualityCallback() ? formatPrice(calcMensualityCallback() as number) : "***" }/{labEcheance}</u><sup>**</sup> ou</span>
                    </div>
                    <span className="price-bold">{formatPrice(calcPriceTTC(totalPrice, vat))}</span>
                </div>
            </div>
        </div>
        
    ) 
}

export default SelectedOptionTable
