import { useMemo } from "react"
import { selectOptions } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import { useAppSelector } from "../../../../../store/hooks/reduxHooks"
import SelectedOptionRow from "./SelectedOptionRow"


export interface OptionsByCategoryListProps {
    categoryKey: string
}

const OptionsByCategoryList = ({categoryKey}: OptionsByCategoryListProps) => {
        const options = useAppSelector(selectOptions)
        const currentLang= useAppSelector(selectCurrentLanguage)

    const optionsByCategory = useMemo( () => {
       return options.filter( el => el.gp_opt === categoryKey ) 
    },[options]) 

    return (
        <div>
        { optionsByCategory && optionsByCategory.map(option => 

                <SelectedOptionRow
                    key={option.ref}
                    option={option}
                />
            )
        }

        </div>
        
    )
}

export default OptionsByCategoryList