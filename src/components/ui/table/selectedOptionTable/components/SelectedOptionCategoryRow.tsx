
import { MouseEventHandler } from 'react'
import { ArrowDownShort, ArrowUpShort, PlusSquare, DashSquare } from 'react-bootstrap-icons'
import CustomBtn from '../../../button/customBtn/CustomBtn'
import { DangerPicto } from '../../../picto/Picto'
import '../selectedOptionTable.scss'

export interface SelectedOptionCategoryRowProps {
   price: string  
   label: string
   handleToggle: MouseEventHandler<HTMLDivElement> 
   expand: boolean
   hasNecessities: boolean
}

const SelectedOptionCategoryRow = ({label, price, expand, hasNecessities, handleToggle}: SelectedOptionCategoryRowProps) => {
    return (
        <div className="d-flex justify-content-between">
            <div className="d-inline-flex">
                <div 
                    className={`option-category text-left my-1" ${hasNecessities && "color-warning"}`}
                >{label}</div>
                
                    <div className="d-flex m-auto  mx-2 "
                        onClick={handleToggle}
                    >
                        {expand ?  <DashSquare className='arrow-svg' size={16} /> : <PlusSquare className='arrow-svg'  size={16}/> }
                    </div>
                
                { hasNecessities && !expand &&
                    <div className="d-flex px-2 align-items-center">
                        <DangerPicto/>
                    </div>
                }
            </div>
            <div className="option-category-price align-self-center">{price}</div>
        </div>
    )
}

export default SelectedOptionCategoryRow