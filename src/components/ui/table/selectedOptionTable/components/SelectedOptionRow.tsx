
import { useCallback, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { OptionRowTransformed } from '../../../../../store/features/api/apiSlice'
import { clearLastOptionSelected, selectLastOption, selectOptionStatusNecessity } from '../../../../../store/features/engineSetting/engineSettingSlice'
import { showPopup } from '../../../../../store/features/modalpopup/modalPopupSlice'
import { selectCurrentLanguage } from '../../../../../store/features/translate/translateSlice'
import { useAppDispatch, useAppSelector } from '../../../../../store/hooks/reduxHooks'
import { formatPrice } from '../../../../../utils/priceUtils'
import CleanDiv from '../../../div/cleanDiv/CleanDiv'
import CustomModal from '../../../modal/customModal/CustomModal'
import NecessityCombine from '../../../modal/_components/necessityCombine/NecessityCombine'
import { DangerPicto } from '../../../picto/Picto'

import '../selectedOptionTable.scss'

export interface SelectedOptionRowProps {
    option: OptionRowTransformed
}


const SelectedOptionRow = ({option}: SelectedOptionRowProps) => {
    const {t} = useTranslation()
    const currentLang= useAppSelector(selectCurrentLanguage)
    const lastOptionAdded = useAppSelector(selectLastOption)
    const necessityInfo = useAppSelector(selectOptionStatusNecessity)
    const dispatch = useAppDispatch()
    const necessityModalId= `necessity-constraint-${option.ref}`

    const handleNecessityInfo = () => {
        dispatch(showPopup(necessityModalId))
    }

    const  optionNecessity = useCallback(()=> {
        const necessity = necessityInfo.filter( el => el.ref === option.ref)  
        if (necessity.length > 0){
            return necessity[0]
        } 
    },[option.ref, lastOptionAdded, necessityInfo])

    // clear the last option after a given time
    useEffect(() => {
        if ( lastOptionAdded && (lastOptionAdded.ref === option.ref)){
                setTimeout(() => {
                    dispatch(clearLastOptionSelected())
                }, 3000)
        }

    }, [lastOptionAdded])

    return (
        <div className={` ${lastOptionAdded && lastOptionAdded.ref == option.ref ? "option-added" : ""} d-flex justify-content-between`}>
            <CustomModal
                id={necessityModalId}
                title={t('cfg_mod_combin')}
                actionsHandledInChildren={true}
            >
                <NecessityCombine 
                    option={option}
                    necessities={optionNecessity()}
                />
            </CustomModal>
            <div 
                className="d-inline-flex"
                onClick={optionNecessity() && handleNecessityInfo}
            >
                { optionNecessity() &&  
                    <div className="px-2 option-necessity-picto">
                        <DangerPicto/>
                    </div>
                }
                    <CleanDiv
                        className={`option-detail-row flex-grow-1 ${optionNecessity() && 'color-warning option-hover'}`} 
                        dangerousHTML={option[currentLang].label+" ("+option.ref+")"}
                    />

            </div>
            <div className="option-detail-row flex-stretch-1">{formatPrice(option.prix)}</div>
        </div>
    )
}

export default SelectedOptionRow