import { useAppSelector } from "../../../../../store/hooks/reduxHooks"
import { OptionRowTransformed, useGetOptionsByModelApiQuery } from "../../../../../store/features/api/apiSlice"
import { selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import { selectOptions } from "../../../../../store/features/engineSetting/engineSettingSlice"

import { arrayIntersectUtils } from "../../../../../utils/arrayUtils"
import { formatPrice } from "../../../../../utils/priceUtils"

import SelectedOptionsAccordion from "./optionsAccordion/SelectedOptionsAccordion"
import LoadingSpinner from "../../../spinner/LoadingSpinner"

// style
import '../selectedOptionTable.scss'

export interface   SelecteOptionsContainerProps {
    modelRef: string
}
const SelectedOptionsContainer = ({modelRef}: SelecteOptionsContainerProps) => {
    const options = useAppSelector(selectOptions) 
    const currentLang = useAppSelector(selectCurrentLanguage)
    let optionsCategory: any
    let content = <div></div>

    // get the cached data 
    optionsCategory = useGetOptionsByModelApiQuery(modelRef, {
        selectFromResult: ({data, isSuccess, isError, isLoading, isUninitialized}) => ({

            categories: data?.categoryRank,
            categoryLabel: data?.categoryOptions,
            data: data,
            isSuccess,
            isError,
            isLoading,
            isUninitialized,
            // order the categories of selected options
            selectedCategories: (selectedOptions: OptionRowTransformed[]): string[] => {
                const categoriesSelectedRef =  [...new Set(selectedOptions.map(option => option.gp_opt))] 
                if (data) {
                    const selectedCategories = arrayIntersectUtils(data.categoryRank, categoriesSelectedRef)
                    return selectedCategories
                }
                return [] 
            },
            // calculate the sum of options price by category
            priceByCategory: (selectedOptions: OptionRowTransformed[], categoryKey: string): number  => {
                const totalPrice = selectedOptions
                    .reduce((prev, next) => {
                        // filter by option group 
                    if( next.gp_opt === categoryKey) return (prev + Math.ceil(next.prix))
                    // if option is not in the right group, don't sum it 
                    return prev
                    }, 0)
                    return totalPrice
            }
        })
    })

    // if the status of the request/cached data recovering  is a success, 
    if(optionsCategory.isError) {
        content = <div>connection error</div>
    } else if (optionsCategory.isUninitialized) {
        content = <div></div>
    } else if (optionsCategory.isLoading) {
        content = (
            <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                <LoadingSpinner/>
            </div>
        ) 
    } else if (optionsCategory.isSuccess) {
        content = (
            <>
                { options && optionsCategory.selectedCategories(options).map((categoryKey: string, index:number) => 
                    <div 
                        key={categoryKey}
                        className={`app-accordion ${(index +1) < optionsCategory.selectedCategories(options).length  && `border-bottom-thin` }`}
                    >
                        <SelectedOptionsAccordion
                            categoryLabel={optionsCategory.categoryLabel[categoryKey][currentLang]}
                            categoryPrice={formatPrice(optionsCategory.priceByCategory(options, categoryKey))}
                            categoryKey={ categoryKey}
                        />
                    </div>
                    )  
                }
            </>
        )
    }

    return (
        <>
            {content}
        </>
    )
}

export default SelectedOptionsContainer