import {Image} from 'react-bootstrap-icons'
import { showPopup } from '../../../../../store/features/modalpopup/modalPopupSlice'
import { useAppDispatch } from '../../../../../store/hooks/reduxHooks'
import { ImagePicto } from '../../../picto/Picto'

import '../selectedOptionTable.scss'

export interface SelectedModelRowProps {
    label: string
    price: string
    modelImageModalId?: string
}
const SelectedModelRow = ({label, price, modelImageModalId}: SelectedModelRowProps) => {
    const dispatch = useAppDispatch()

    const handleImageClick = () => {
       modelImageModalId &&  dispatch(showPopup(modelImageModalId))
    }

    return (
        <div className="d-flex justify-content-between py-2">
            <div className="d-inline-flex">
                <h4 className="option-model  text-left my-0">{label}</h4>
                { modelImageModalId &&
                    <ImagePicto
                        onClick={handleImageClick}
                    />
                }
            </div>
            <h4 className="price-bold text-right my-0">{price}</h4>
        </div>
    )
}

export default SelectedModelRow