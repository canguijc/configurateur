import { useCallback, useEffect, useState } from "react"
import { selectLastOption, selectOptionStatusGroupNecessity } from "../../../../../../store/features/engineSetting/engineSettingSlice"
import { useAppSelector } from "../../../../../../store/hooks/reduxHooks"
import OptionsByCategoryList from "../OptionsByCategoryList"
import SelectedOptionCategoryRow from "../SelectedOptionCategoryRow"

export interface SelectedOptionsAccordionProps {
    categoryLabel: string 
    categoryPrice: string
    categoryKey: string
}
const SelectedOptionsAccordion = ({categoryLabel, categoryPrice, categoryKey}: SelectedOptionsAccordionProps) => {
  const [visible, setVisible] = useState(true)
  const currentOption = useAppSelector(selectLastOption)
  const necessityGroup = useAppSelector(selectOptionStatusGroupNecessity)

  const groupHasNecessities = useCallback(() => {
    return necessityGroup.includes(categoryKey)
  },[necessityGroup, categoryKey])

  // automatically open the accordion when an option of a category is added
  useEffect( () => {
    if (currentOption?.gp_opt === categoryKey  )
        setVisible(true)
  },[currentOption])

  const handleToggleVisible = () => {
    setVisible(visible => !visible)

  }

 return (
    <div className="py-1">
        <SelectedOptionCategoryRow
            label={categoryLabel}
            price={categoryPrice}
            handleToggle={handleToggleVisible}
            expand={visible}
            hasNecessities={groupHasNecessities()}
        />
        { visible && 
            <div>
                <OptionsByCategoryList categoryKey={categoryKey}/>
            </div>
        }
    </div>
 )
}

export default SelectedOptionsAccordion