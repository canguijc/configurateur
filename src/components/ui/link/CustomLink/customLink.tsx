import { HTMLAttributes } from "react"
import { AlertPicto, ContinuePicto, InfoPicto, LogoutPicto } from "../../picto/Picto"

// styles
import './customLink.scss'

export interface CustomLinkProps extends HTMLAttributes<HTMLDivElement> {
     text?: string
}

/* todo: implement toggle state to show or hide the modal */
export const InfoLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div 
            className="cus-link-info d-inline-flex"
            {...divProps}
        >
            {/* <InfoModal/> */}
            <InfoPicto/>
            { text &&
                <div className="align-self-center px-1">
                    <small><em><u>{text}</u></em></small>
                </div> 
            }
        </div>
    )
}

export const InCompatibilityLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div 
            className="cus-link-compat d-inline-flex"
            {...divProps}
        >
            <AlertPicto/>
            { text && 
                <div className="align-self-center px-1">
                    <small><em><u>{text}</u></em></small>
                </div>
            }
            </div>
    )
}


export const RetrieveConfigurationLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div 
            className="cus-link-compat d-inline-flex"
            {...divProps}
        >
            <ContinuePicto/>
            { text && 
                <div className="cus-config-link align-self-center px-1">
                    <span className="text-nowrap">{text}</span>
                </div>
            }
            </div>
    )
}

export const AdminConfigurationLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div
            className="cus-link-compat d-inline-flex ptH"
            {...divProps}
        >
            <ContinuePicto/>
            { text &&
                <div className="cus-config-link align-self-center px-1">
                    <a href="/configurateur/reassign.php" className="text-nowrap cus-config-link noU">{text}</a>
                </div>
            }
            </div>
    )
}

export const DeleteConfigurationLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div 
            className="cus-link-delete d-flex align-items-center cbtn-outline btn  rounded-0"
            {...divProps}
        >
            { text && 
                <div className="px-1">
                    {text}
                </div>
            }
            </div>
    )
}

export const LogoutLink = ({text, ...divProps}: CustomLinkProps) => {
    return (
        <div 
            className="cus-link-logout d-inline-flex"
            {...divProps}
        >
            { text && 
                <div className="align-self-center px-1">
                    <h5>{text}</h5>
                </div>
            }
            <LogoutPicto/>
        </div>
    )
}

export const CancelConfLink = ({text, ...divProps}:CustomLinkProps) => {
    return (
        <div
            className="cus-backlink-regular mx-2 d-inline-flex"
            {...divProps}
        >
            <u>{text}</u>
        </div>
    )
}