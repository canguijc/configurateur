import { DetailedHTMLProps, FunctionComponent, HTMLAttributes, ImgHTMLAttributes } from 'react'
import alertPicto from '../../../assets/pictos/p_alerte.svg'
import infoPicto from '../../../assets/pictos/p_information.svg'
import continuePicto from '../../../assets/pictos/p_reprendre.svg'
import logoutPicto from '../../../assets/pictos/p_logout.svg'
import dangerPicto from '../../../assets/pictos/p_alerte_red.svg'

// styles
import './picto.scss'
import { Icon, IconProps, Image } from 'react-bootstrap-icons'

export interface PictoProps extends DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement> {}
export interface ImagePictoProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const AlertPicto = ()  => {
    return (
        <img className="picto" src={alertPicto} alt={"alert"}/>
    )
}

const InfoPicto = ()  => {
    return (
        <img className="picto" src={infoPicto} alt={"info"}/>
    )
}

const ContinuePicto = ()  => {
    return (
        <img className="picto-continue mx-2" src={continuePicto} alt={"continue"}/>
    )
}

const LogoutPicto = ()  => {
    return (
        <img className="logout-picto" src={logoutPicto} alt={"logout"}/>
    )
}


const ImagePicto = ({...imgProps}: ImagePictoProps)  => {
    return (
        <div
            className='d-flex px-2 align-items-center'
            {...imgProps}
        >
            <Image className="image-picto" size={20} 
            /> 
        </div>
    )
}

const DangerPicto = ({...imgProps}: PictoProps)  => {
    return (
        <img 
            className="picto" 
            src={dangerPicto} 
            alt={"dang"}
            {...imgProps}

        />
    )
}

export {AlertPicto, InfoPicto, ContinuePicto, LogoutPicto, DangerPicto, ImagePicto}