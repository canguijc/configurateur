import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { CategoryRank, fetchedCategoryOptionsRow } from '../../../../store/features/api/apiSlice'
import { selectOptionStatusNecessity } from '../../../../store/features/engineSetting/engineSettingSlice'
import { selectCurrentLanguage } from '../../../../store/features/translate/translateSlice'
import { useAppSelector } from '../../../../store/hooks/reduxHooks'

// style
import './dropUpButton.scss'


interface DropUpButtonProps extends  React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{
    categoryRank: CategoryRank
    currentOptionCategory: string  
    categoryOptionLabel: fetchedCategoryOptionsRow
    handleSelectCategory: ( category: string) => void 
    handleSelectResumeConfig: () => void
}


const DropUpButton = ({categoryRank, currentOptionCategory, categoryOptionLabel, handleSelectCategory, handleSelectResumeConfig, ...buttonProps}: DropUpButtonProps) => {
    const {t} = useTranslation()
    const currentLang= useAppSelector(selectCurrentLanguage)
    const necessities = useAppSelector(selectOptionStatusNecessity)


    // return the step of the current category  
    const steps = useMemo(() => {
        return ( currentOptionCategory ? `${categoryRank.indexOf(currentOptionCategory)+ 1}/ ${categoryRank.length}`: "")
    }, [currentOptionCategory, categoryOptionLabel])
    const cur:number=parseInt(currentOptionCategory)
    return (
        <div className="btn-group dropup">
        <button type="button" className="dup-btn-outline btn dropdown-toggle rounded-0" data-bs-toggle="dropdown" aria-expanded="false" {...buttonProps} >
            { currentOptionCategory && 
                  // steps  and category name
                `${steps} ${currentOptionCategory ? categoryOptionLabel[Number(currentOptionCategory)][currentLang]: ""} `
            } 
        </button>
            <ul className="dropdown-menu">
                {/* here goes the logic to iterate over options categories */}
                {categoryRank && categoryRank.map( (category, index) =>
                    <li
                        key={category}
                        id={index.toString()}
                        //className="dup-item d-block py-1 px-2"
                        className={`${cur==index+1 ? "dup-item d-block py-1 px-2 selectLi":"dup-item d-block py-1 px-2"} `}
                        onClick={(e) => handleSelectCategory(category)}
                    >
                        { category ? categoryOptionLabel[category][currentLang] : "" }
                    </li>
                )}
                <li
                 className={`${necessities && necessities.length > 0 ? "not-allowed-cursor" : undefined }`}
                >
                    <div
                        className={`dup-item d-block py-1 px-2 ${necessities && necessities.length > 0 && "disable-row"}`}
                        onClick={handleSelectResumeConfig}
                        // aria-disabled={necessities && necessities.length > 0} 
                    >
                        {t('cfg_resume_config')}
                    </div>
                </li>
            </ul>
        </div>
    )
}

export default DropUpButton