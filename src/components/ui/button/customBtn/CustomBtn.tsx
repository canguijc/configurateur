import React from 'react'

import './customBtn.scss'

interface BtnValidProps extends  React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>{
    label: string
    btnType?: 'outline' | 'plain'
}

const CustomBtn = ({label, btnType='plain', ...buttonProps}: BtnValidProps) => {
    // different styling  for props btnType
    const btnStyle = btnType === 'outline' ? "cbtn-outline btn  rounded-0 px-4" : "cbtn-plain btn rounded-0 px-4"
    // const btnStyle = btnType === 'outline' ? "btn  btn-outline-success  rounded-0 px-4" : "btn btn-success rounded-0 px-4"
    return (
        <button {...buttonProps} className={`${btnStyle} mx-1`}>{label}</button>
    )
}

export default CustomBtn