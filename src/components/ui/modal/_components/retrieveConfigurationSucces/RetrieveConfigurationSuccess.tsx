
export interface RetrieveConfigurationSuccessProps {
    text: string
}

const RetrieveConfigurationSuccess = ({text}: RetrieveConfigurationSuccessProps) => {
    return (
        <div className="d-flex font-regular mt-4 mb-4">
            {text}
        </div>
    )
}

export default RetrieveConfigurationSuccess