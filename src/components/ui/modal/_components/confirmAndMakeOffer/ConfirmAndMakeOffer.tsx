import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import CustomBtn from "../../../button/customBtn/CustomBtn"
import { useTranslation } from "react-i18next"
import appConfig from "../../../../../appconfig"
import { selectSaveConfiguration } from "../../../../../store/features/saveConfiguration/saveConfigurationSlice"
import { useNavigate } from "react-router-dom"
import useAppClearStore from "../../../../../store/hooks/useAppClearStore"
import { changeLanguage, selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import {useEffect, useState} from "react";
import { getUserRole } from '../../../../../helpers/sessionStoragehelpers';

import '../modalComponents.scss'
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice"

const ConfirmAndMakeOffer = () => {
    const {t} = useTranslation()
    const dispatch = useAppDispatch()
    const clearAppStore = useAppClearStore()
    const selectSavedConfiguration = useAppSelector(selectSaveConfiguration)
    const gammesPagePath = appConfig.pages.gammes.link.abs
    const currentLang = useAppSelector(selectCurrentLanguage)






    const [active,setActive]=useState(false);
    async function fetchData() {
        const data2 = await  getUserRole().then(async r=>{
            // @ts-ignore
            //setRole(r)
            return r
        });
        return Promise.all([data2]).then(( [data2]) => {
            if(data2!="ROLE_RESPONSABLE_VENTES"){

                setActive(true);
            }
            // setAllData(categorizeFiches(data2,data1));

        });
    }
    useEffect(() => {

        fetchData().then(async r=>{

            // setActive(true)
        })

        //setAllData(categorizeFiches(categories,fiches));

    }, []);
    console.log("active",active)
    const openInNewTab = (url:string, idConfig: string) => {
        window.open(`${url}${idConfig}&langue=${currentLang}`, '_blank', 'noopener,noreferrer');
    };

    const handleMakeOffer = () => {
        if (selectSavedConfiguration.id){
            openInNewTab(appConfig.api.makeOfferUrl,selectSavedConfiguration.id)        
            clearAppStore()
            dispatch(hidePopup())
        }
        clearAppStore()
        dispatch(hidePopup())
    }

    const handleClose = () => {
        clearAppStore()
        dispatch(hidePopup())
    }

    return (
        <div>
            <div className="font-regular">{t("cfg_conf_sauveg_msg")}</div>
            {/* footer */}
            <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                {/* if an id for a saved configuration exist user can make offer */}
                { selectSavedConfiguration.id && active &&
                    <CustomBtn
                        label={t("cfg_faire_offr_btn").toUpperCase()}
                        onClick={handleMakeOffer}
                    />
                }
                <CustomBtn 
                    label={t("cfg_annul_btn").toUpperCase()}
                    onClick={handleClose}
                />
            </div>
        </div>
    )
}

export default ConfirmAndMakeOffer