import { useTranslation } from "react-i18next"
import appConfig from "../../../../../appconfig"
import { ConfigurationItems, Model, useUpdateConfigurationMutation } from "../../../../../store/features/api/apiSlice"
import { hidePopup, showPopup } from "../../../../../store/features/modalpopup/modalPopupSlice"
import { selectTotalOptionsPrice, selectTotalConfigurationPrice, selectconfiguration, selectOptionsRef, selectModel } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { useSaveConfigurationMutation} from "../../../../../store/features/api/apiSlice"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import CustomBtn from "../../../button/customBtn/CustomBtn"

import { formatPrice } from "../../../../../utils/priceUtils"

// style
import '../modalComponents.scss'
import { SubmitHandler, useForm } from "react-hook-form"
import { ModalConfirmationDefaultId } from "../../../../confirmationModal/ConfirmationModal"
import { useCallback, useEffect, useState } from "react"
import LoadingSpinner from "../../../spinner/LoadingSpinner"
import { getJwtInSessionHelper } from "../../../../../helpers/sessionStoragehelpers"
import { clearSaveConfiguration, selectSaveConfiguration, setSaveConfigurationId, setSaveConfigurationName } from "../../../../../store/features/saveConfiguration/saveConfigurationSlice"
import { selectRetrieveConfigurationData, selectRetrieveStatus } from "../../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"

type FormInput = {
    configName: string
}

export interface SaveConfigurationProps {
    model: Model
}

const SaveConfiguration = ({model}: SaveConfigurationProps) => {
    const [errConfId, setErrConfId] = useState(false)
    const {t} = useTranslation()
    const dispatch = useAppDispatch()    
    const [saveConfTrigger, saveConfResponse] = useSaveConfigurationMutation() 
    const [updateConfTrigger, updateConfResponse] = useUpdateConfigurationMutation() 
    const retrievedConfigurationData = useAppSelector(selectRetrieveConfigurationData)
    const retrievedConfigurationStatus = useAppSelector(selectRetrieveStatus)
    const servicesTotalPrice = useAppSelector(selectTotalOptionsPrice)
    const isConfigurationSaved = typeof useAppSelector(selectSaveConfiguration).id === 'string' 
    const optionsSelectedRef = useAppSelector(selectOptionsRef)
    const confTotalPrice = useAppSelector(selectTotalConfigurationPrice)
    const configurationInfo = useAppSelector(selectconfiguration)
    const currency = appConfig.app.defaultCurrency
    const confirmSavedModalId: ModalConfirmationDefaultId = "modalConfigHasBeenSaved"
    const { register, handleSubmit, formState: { errors } } = useForm<FormInput>();

    // check if options of a retrieved configuration have been changed
    const isRetrievedConfigurationModified = useCallback(() => {
        // stringify the retrieved options
        const retrievedOptionsSorted = retrievedConfigurationData && retrievedConfigurationData.conf.options.sort().join(",")  
        // stringify the selected options
        const selectedOptionsSorted = configurationInfo.options.optionsStatus.selectedOptionsRef && configurationInfo.options.optionsStatus.selectedOptionsRef.sort().join(",")  
        // compare the 2 lists
        const isOptionsUnchanged = (retrievedOptionsSorted == selectedOptionsSorted)
        // compare the model in retrieved conf and the one in configuration
        const isModelUnchanged = retrievedConfigurationData && (retrievedConfigurationData.conf.model.ref === model.ref)
        const isGammeUnchanged = retrievedConfigurationData && (retrievedConfigurationData.conf.gamme.ref === configurationInfo.gamme?.reference) 
        return isGammeUnchanged && isModelUnchanged && isOptionsUnchanged 
    }, [configurationInfo, retrievedConfigurationData])

    useEffect(() => {
        if (saveConfResponse.isSuccess){
            // is there an id of the saved configuration in the response?
            if (saveConfResponse.data.id_conf){
                // keep the id of the saved configuration
                dispatch(setSaveConfigurationId(saveConfResponse.data.id_conf))
            }
            dispatch(hidePopup())
            dispatch(showPopup(confirmSavedModalId))
        }

        if (updateConfResponse.isSuccess){
            // is there an id of the saved configuration in the response?
            if (updateConfResponse.data.id_conf){
                // keep the id of the saved configuration
                dispatch(setSaveConfigurationId(updateConfResponse.data.id_conf))
            }
            dispatch(hidePopup())
            dispatch(showPopup(confirmSavedModalId))
        }

        // managing errors
        if (saveConfResponse.isError || updateConfResponse.isError || errConfId) {
            dispatch(clearSaveConfiguration())
        }
    }, [saveConfResponse, updateConfResponse, errConfId])

    const handleClose = () => {
        dispatch(hidePopup())
    }
    const onSubmit: SubmitHandler<FormInput> = (data, event) => {
        event?.preventDefault()
        const jwt = getJwtInSessionHelper()

        /* save configuration */
        const saveConfigurationFilterData: ConfigurationItems = {
            gamme: {
                label: configurationInfo.gamme ? configurationInfo.gamme.gamme : "",
                ref: configurationInfo.gamme ? configurationInfo.gamme?.reference : ""
            },
            model: {
                label: configurationInfo.model ? configurationInfo.model?.label : "",
                ref: configurationInfo.model ? configurationInfo.model.ref : "",
                prix: configurationInfo.model ? configurationInfo.model.prix : undefined
            },
            options: optionsSelectedRef,
            total: configurationInfo.total
        }
        const saveConfigBody = {
           jwt: jwt ? jwt : "", 
           name: data.configName ,
           configuration: saveConfigurationFilterData
        }

        if(retrievedConfigurationStatus === "succeeded") {
        // update configuration
            const configurationId = retrievedConfigurationData?.id_save_conf 
            // if a configuration id is found update data else set error 
            configurationId ? updateConfTrigger({...saveConfigBody, id_conf:configurationId}) : setErrConfId(true)
        } else {
            // save configuration
            saveConfTrigger(saveConfigBody) 
        }
        dispatch(setSaveConfigurationName(data.configName))
    } 

    return (
        <div className="m-2">
            <h6 className="font-bold modal-section-border-bottom pb-1">{t('cfg_current_config')}</h6>
                <table className="w-100 pb-2 mb-2">
                    <tbody>
                        {/* model */}
                        <tr className="spec-model-line" >
                            <td className="text-start font-regular" colSpan={6}>{model.label}</td>
                            <td className="text-center font-regular">{currency.toUpperCase()}</td>
                            {/* <td className="text-center font-regular">chf</td> */}
                            <td className="text-end font-regular">{`${formatPrice(model.prix)}(${t("cfg_price_ht").toUpperCase()})`}</td>
                        </tr> 
                        {/* options */}
                        <tr className="spec-model-line" >
                            <td className="text-start font-regular" colSpan={6}>{t('cfg_opt_access')}</td>
                            <td className="text-center font-regular">{currency.toUpperCase()}</td>
                            <td className="text-end font-regular">{`${formatPrice(servicesTotalPrice)}(${t("cfg_price_ht").toUpperCase()})`}</td>
                        </tr> 
                        {/* total */}
                        <tr className="spec-model-line" >
                            <td className="text-start" colSpan={6}></td>
                            <td className="text-center font-bold" >{currency.toUpperCase()}</td>
                            <td className="text-end font-bold">{`${formatPrice(confTotalPrice)}(${t("cfg_price_ht").toUpperCase()})`}</td>
                        </tr> 
                    </tbody>
                </table>
                {/* name config */}
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row" >
                        <div className="col-6 align-top font-bold pt-4">
                                {t('cfg_name_config')}
                        </div>
                        <div className="col-6 text-end font-bold pt-4" >
                            <input 
                                onKeyDown={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                                className="font-regular"
                                type="text" defaultValue={retrievedConfigurationData ? retrievedConfigurationData.name : ""} 
                                {...register('configName', 
                                    {required: true, minLength:4, maxLength: 15 ,  pattern: /^[[A-Za-z0-9]+[A-Za-z0-9-_\. ]+$/i })} 
                            />
                            { errors.configName && 
                                <p className="d-inline-block color-warning text-justify pl-3 py-0 my-0 font-regular">
                                    <small>
                                        {`${t("cfg_entrer_nom_conf_msg")} 4 ${t("cfg_con_et")} 15 ${t("cfg_caracteres")}: azAZ09 . -_`} 
                                    </small>
                                </p>
                            }
                        </div>
                    </div> 
                    {/* saving configuration status */}
                    <div className="d-flex m-2 p-2 justify-content-center">
                        { /* post configuration state */}
                        {  (saveConfResponse.isLoading || updateConfResponse.isLoading) &&  
                            <LoadingSpinner/>
                        }
                        {  (saveConfResponse.isError || updateConfResponse.isError || errConfId) &&  
                            <div className="font-regular color-warning">{t("cfg_err_enregist_conf_msg")}</div>
                        }
                    </div>

                    {/* footer */}
                    <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                        <CustomBtn 
                            label={ t("cfg_annul_btn").toUpperCase()}
                            onClick={handleClose}
                        />

                        <CustomBtn
                            // label={ retrievedConfigurationStatus === "succeeded" ? t("cfg_update_conf").toUpperCase() : t("cfg_save_conf").toUpperCase()}
                            label= {t("cfg_save_conf").toUpperCase()}
                            disabled={isConfigurationSaved}
                            type="submit"
                        />
                    </div>
                </form>
        </div>

    )
}

export default SaveConfiguration