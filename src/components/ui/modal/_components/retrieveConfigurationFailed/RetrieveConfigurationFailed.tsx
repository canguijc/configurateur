import { t } from "i18next"
import { useTranslation } from "react-i18next"
import { selectRetrieveErrors } from "../../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppSelector } from "../../../../../store/hooks/reduxHooks"

import '../modalComponents.scss'

const RetrieveConfigurationFailed = () => {
    const {t} = useTranslation()
    const retrieveErrors = useAppSelector(selectRetrieveErrors)

    return (
        <div>
            <div className="font-regular">{t("cfg_reprise_impossible_msg")}</div>
            { (retrieveErrors.optionsWithIncompatibility  ?  !retrieveErrors.optionsNotInList : retrieveErrors.optionsNotInList) &&
                <div className="font-regular">{t("cfg_reprise_contrainte_err_msg")}.</div>
            }
        </div>

    )
}

export default RetrieveConfigurationFailed