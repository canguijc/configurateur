import { Fragment, useEffect, useState } from 'react'
import appConfig, { Language } from '../../../../../appconfig'
import SpecArray, { SpecRowData } from './components/SpecRowData'

// styles
import './modelSpecList.scss'
import LoadingSpinner from '../../../spinner/LoadingSpinner'
import SpecCategoriesList, { SpecCategoriesData } from './components/SpecCategoriesList'
import { useAppSelector } from '../../../../../store/hooks/reduxHooks'
import { selectCurrentLanguage } from '../../../../../store/features/translate/translateSlice'

type ModelSpecData = {[key in Language]: SpecCategoriesData }

type FetchedSpecData  = {
    loading: 'pending'| 'succeeded' |'failed', 
    data: ModelSpecData | null
}
interface ModelSpecListProps {
    modelLabel: string
}

const initialState: FetchedSpecData = {loading:'pending', data: null }

/**
 * label is  used to fetch model specs  data
 *  modelLabel: parameter label of the engine model needed
 */
const ModelSpecList = ({modelLabel}: ModelSpecListProps) => {
    // set initial state
    const [specData, setSpecData] = useState<FetchedSpecData>(initialState)
    // get the default url to fetch specs
    const baseSpecUrl = appConfig.api.baseModelSpecUrl
    const currentLang = useAppSelector(selectCurrentLanguage)

    let content
    
    // fetch the data
    useEffect(() => {
        fetch(`${baseSpecUrl}${modelLabel}`)
            .then(res => res.json())
            .then(data => {
                setSpecData({loading:'succeeded', data})
        }) 
            // need to process errors
            .catch(err => setSpecData({loading:'failed', data: null}))
    }, [])

    if (specData.loading === 'pending'){
        content = (
               <div style={{height:"30vh"}}>
                    <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                        <LoadingSpinner/>
                    </div>
               </div> 
        )
    } else if (specData.loading === 'failed'){
       content = <div>erreur de chargement...</div>
    } else if (specData.loading === 'succeeded') {
        content = (
                <table className="m-3">
                    <tbody>
                    { specData && 
                        <SpecCategoriesList categories={ specData.data  ?  specData.data[currentLang] : null }/>                  
                    }
                    </tbody>
                </table>
        )
    }
    return (
        <div>
            <fieldset className="scheduler-border m-3 ">
                <legend className="scheduler-border float-none w-auto p-2">
                    <h5 className="spec-model-label">{modelLabel}</h5>
                </legend>
                {content}
            </fieldset>
        </div>
    )
}

export default ModelSpecList