
//styles 
import '../modelSpecList.scss'

export type SpecRowData = { lib:string, valeur: string}

const SpecArray = ({specs}:{specs: SpecRowData[] | null}) => {
    if (specs === null )
        return (null) 

    return (
        <>
            {specs.map((spec, index) => {
                return (
                    <tr className="spec-model-line" key={index}>
                        <td className="pe-2" colSpan={2}>{spec.lib}</td>
                        <td className="ps-1">{spec.valeur}</td>
                    </tr> 
                )} 
            )}
        </>
    )
}

export default SpecArray