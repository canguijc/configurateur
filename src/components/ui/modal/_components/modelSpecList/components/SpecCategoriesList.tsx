
//styles 
import { Fragment } from 'react'
import '../modelSpecList.scss'
import SpecArray, { SpecRowData } from './SpecRowData'

export type SpecCategoriesData = {[key:string]:SpecRowData [] | null}
export type SpecCategoriesListProps= {
    categories: SpecCategoriesData  | null
}
const SpecCategoriesList = ({categories}: SpecCategoriesListProps) => {
    return (
        <>
            {/* print each spec category */}
            { categories &&  Object.keys(categories).map( category => 
                <Fragment key={category}>
                    <tr className="cat-border-bottom px-2 pb-1 pt-3">
                        <td className="spec-model-category pt-2"><h5 className="mb-0">{category}</h5></td>
                        
                    </tr>  
                    {/* print  specs of each category */}
                    {categories && <SpecArray specs={categories[category]} />}
                </Fragment>
            )}
        </>
    )
}

export default SpecCategoriesList