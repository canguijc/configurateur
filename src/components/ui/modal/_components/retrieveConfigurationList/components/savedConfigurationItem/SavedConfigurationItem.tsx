import { useTranslation } from "react-i18next"
import { useNavigate } from "react-router-dom"
import appConfig from "../../../../../../../appconfig"
import { SavedConfigDataItem } from "../../../../../../../store/features/api/apiSlice"
import { setDeleteConfigurationData } from "../../../../../../../store/features/deleteConfiguration/deleteConfigurationSlice"
import { hidePopup, showPopup } from "../../../../../../../store/features/modalpopup/modalPopupSlice"
import { setRetrieveConfigurationData } from "../../../../../../../store/features/retrieveConfiguration/retrieveConfigurationSlice"
import { useAppDispatch } from "../../../../../../../store/hooks/reduxHooks"
import { formatPrice } from "../../../../../../../utils/priceUtils"
import { ModalConfirmationDefaultId } from "../../../../../../confirmationModal/ConfirmationModal"
import CustomBtn from "../../../../../button/customBtn/CustomBtn"
import { DeleteConfigurationLink } from "../../../../../link/CustomLink/customLink"


export interface SavedConfigurationItemProps {
    savedConfig: SavedConfigDataItem 
}

const SavedConfigurationItem = ({savedConfig}: SavedConfigurationItemProps) => {
    const {t} = useTranslation()
    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const currency = appConfig.app.defaultCurrency
    const retrievePagePath = appConfig.pages.retrieveConfig.link.abs
    const confirmDeleteModalId: ModalConfirmationDefaultId =  "modalConfirmDeleteConf"

    const handleRetrieve = () => {
        // set  data of saved config in retrieveConfigSlice state
        dispatch(setRetrieveConfigurationData(savedConfig))
        // hide this popup
        dispatch(hidePopup())
        // go to the retrievePage
        navigate(retrievePagePath)
    }
    const handleDelete = () => {
        dispatch(setDeleteConfigurationData(savedConfig))
        dispatch(showPopup(confirmDeleteModalId))
    }

    return (
        <div
            className="modal-section-border-bottom w-100 my-2 pb-2"
        >
            <table 
                className="w-100"
            >
                <tbody
                >
                    {/* config name */}
                    <tr>
                        <td className="text-start dateConf " colSpan={8}>{savedConfig.dateCreAt.substring(8)}-{savedConfig.dateCreAt.substring(5,7)}-{savedConfig.dateCreAt.substring(0,4)}</td>
                    </tr>
{/*
                    <tr>
                        <td className="text-start" colSpan={8}>{`${savedConfig.prenom}`} {savedConfig.nom} - {savedConfig.agent}</td>
                    </tr>
*/}
                    {/* gamme */}
                    <tr className="spec-model-line font-bold borderBot" >
                        <td className="text-start" colSpan={6}>{`${savedConfig.name}`} <span className="font-regular">({`${savedConfig.conf.gamme.label}`})</span></td>
                        <td className="text-center">{currency.toUpperCase()}</td>
                        <td className="text-end ">{`${formatPrice(savedConfig.conf.total.totalConfigurationPrice)}(${t("cfg_price_ht").toUpperCase()})`}</td>
                    </tr> 
                    {/* model */}
                    <tr className="spec-model-line borderMT" >
                        <td className="text-start font-regular" colSpan={6}>{savedConfig.conf.model.label}</td>
                        <td className="text-center font-regular">{currency.toUpperCase()}</td>
                        <td className="text-end font-regular">{`${savedConfig.conf.model.prix ? formatPrice(savedConfig.conf.model.prix) : ""}(${t("cfg_price_ht").toUpperCase()})`}</td>
                    </tr> 
                    {/* options */}
                    <tr className="spec-model-line" >
                        <td className="text-start font-regular" colSpan={6}>{t('cfg_opt_access')}</td>
                        <td className="text-center font-regular">{currency.toUpperCase()}</td>
                        <td className="text-end font-regular">{`${formatPrice(savedConfig.conf.total.totalOptionsPrice)}(${t("cfg_price_ht").toUpperCase()})`}</td>
                    </tr> 
                </tbody>
            </table>

            <div className="d-flex flex-row justify-content-between mt-4 mb-4">
                <DeleteConfigurationLink
                    text={t('cfg_delete_btn')}
                    onClick={handleDelete}
                />
                <CustomBtn
                    label={t('cfg_retrieve_btn').toUpperCase()}
                    onClick={handleRetrieve}
                />
            </div>
        </div>
        
    )
}

export default SavedConfigurationItem