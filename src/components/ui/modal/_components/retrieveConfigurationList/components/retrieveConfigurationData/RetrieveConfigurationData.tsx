import { useTranslation } from "react-i18next"

import { useAppDispatch } from "../../../../../../../store/hooks/reduxHooks"
import { hidePopup } from "../../../../../../../store/features/modalpopup/modalPopupSlice"
import { useGetSavedConfigurationByUserQuery } from "../../../../../../../store/features/api/apiSlice"

import SavedConfigurationItem from "../savedConfigurationItem/SavedConfigurationItem"
import CustomBtn from "../../../../../button/customBtn/CustomBtn"

import appConfig from "../../../../../../../appconfig"
import LoadingSpinner from "../../../../../spinner/LoadingSpinner"
import { useEffect } from "react"


export interface RetrieveConfigurationDataProps {
    userId: string
}

const RetrieveConfigurationData = ({userId}: RetrieveConfigurationDataProps) => {
    // TODO: to remove only for testing
    // let savedConfigList: SavedConfigDataItem[] = []

    const {t} = useTranslation()
    const dispatch = useAppDispatch()
    const savedConfigurations = useGetSavedConfigurationByUserQuery(userId)
    const currency  = appConfig.app.defaultCurrency
    let content: any
    // list of user configuration fetched 
    // const savedConfigList: UserConfigSaved[] = []

    // refetch data everytime RetrieveConfigurationData is mounted 
    useEffect(() => {
        savedConfigurations.refetch()
    }, [])

    const handleClose = () => {
        dispatch(hidePopup())
    }

    if (savedConfigurations.isError) {
        // content = <Navigate to="/"/>
        content = <div>{t("cfg_err_chargement")}</div>
    }  else if (savedConfigurations.isUninitialized) {
        content = <div></div>
    }  else if (savedConfigurations.isLoading) {
        content = (
               <div style={{height:"30vh"}}>
                    <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                        <LoadingSpinner/>
                    </div>
               </div> 
        )
    } else if (savedConfigurations.isSuccess) {
        content = (
            <>
                {/* <h6 className="font-bold modal-section-border-bottom pb-1">{t('cfg_current_config')}</h6> */}
                {/* { savedConfigList && savedConfigList.length > 0  
                    ? savedConfigList.map((savedConfig, index) => ( */}
                { savedConfigurations.data && savedConfigurations.data.length > 0  
                    ? savedConfigurations.data.map((savedConfig, index) => (
                    <SavedConfigurationItem
                            key={index} 
                            savedConfig={savedConfig}
                    />
                    ))
                    : <div>{t("cfg_no_save_conf_msg")}</div>
                }
            </>
        )
    }

    return (
        <div className="m-2">
            {content}
            {/* footer */}
            <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                <CustomBtn
                    label={t("cfg_annul_btn").toUpperCase()}
                    onClick={handleClose}
                />
            </div>
        </div>
    )
}

export default RetrieveConfigurationData