import { SavedConfigDataItem } from "../../../../../store/features/api/apiSlice"
import RetrieveConfigurationData from "./components/retrieveConfigurationData/RetrieveConfigurationData"
import { getUserIdInSessionStoragehelper} from '../../../../../helpers/sessionStoragehelpers'
import CustomBtn from "../../../button/customBtn/CustomBtn"
import { useAppDispatch } from "../../../../../store/hooks/reduxHooks"
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice"
import { useTranslation } from "react-i18next"

export interface RetrieveConfigurationProps {
}
const RetrieveConfigurationList = () => {

    const {t} = useTranslation()
    const dispatch = useAppDispatch()

    const handleClose = () => {
        dispatch(hidePopup())
    }
    var userId:any="";
    if(import.meta.env.PROD) userId = getUserIdInSessionStoragehelper()
    else userId = 9

    if(userId){
        return ( <RetrieveConfigurationData userId={userId} />)
    } else {
        {/* error while processing decode jwt payload */}
        return (
            <div>
                <div className="m-2">{t("cfg_conf_save_list_err_msg")}</div>

                {/* action */}
                <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                    <CustomBtn
                        label={t("cfg_annul_btn").toUpperCase()}
                        onClick={handleClose}
                    />
                </div>
            </div>
        )
    }
}

export default RetrieveConfigurationList