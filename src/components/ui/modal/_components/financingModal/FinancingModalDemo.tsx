import { ChangeEvent, useEffect, useState } from "react"

type dataState = {
    status: 'pending' | 'error' | 'success'
    data: unknown
}

const FinancingModalDemo = () => {
    const [rate, setRate] = useState<string | undefined>(undefined)
    const [data, setData] = useState<dataState>({status:'pending', data: {}}) 
    const handleChangeRate = (e: ChangeEvent<HTMLInputElement> ) => {
        setRate(e.target.value)
    }
    
    useEffect(() => {
        fetch('ps://merlo-ch.com/gestion/configurateur/ajax_get_options_modele.php?reference=MA00839')
            .then(data => data.json())
            .then( res => setData({status: 'success', data: res}) )
            .catch(err => setData({status: 'error', data: {}}))
    }, [])


    if (data.status === 'pending') {
        return <div> chargement...</div>
    } else if (data.status === 'success') {
        return (
            <div>
                module de financement 
                <input 
                    type="text"
                    onChange={handleChangeRate}
                    />
                <div>{rate}</div> 
            </div>
        )
    } else {
        return (<div>error</div>)
    }
}

export default FinancingModalDemo