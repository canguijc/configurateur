import { useCallback, useEffect, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import appConfig from "../../../../../appconfig"
import { selectTotalConfigurationPrice } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { FinancingState, selectFinancing, setFinancing } from "../../../../../store/features/financing/financingSlice"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import { calcPriceTTC, formatPrice } from "../../../../../utils/priceUtils"
import CustomBtn from "../../../button/customBtn/CustomBtn"
import { useTranslation } from "react-i18next"
import { calcMensuality } from "../../../../../helpers/financingHelpers"
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice"

type FinancingVariable = {
    rate: string
    durationMonth: string
    apport : string
    echeance : string
}
const FinancingModal = () => {
    const {t} = useTranslation()
    const [mensuality, setMensuality] = useState(0)
    const [rateError, setRateError] = useState(false)
   // const [durationError, setdurationError] = useState(false)
    const [apportError, setapportError] = useState(false)
    const [echeanceError,setecheanceError] = useState(false)
    const dispatch = useAppDispatch()
    const currentFinancing = useAppSelector(selectFinancing)
    const totalConfPrice = useAppSelector(selectTotalConfigurationPrice) 
    const { register, handleSubmit, watch, formState: { errors } } = useForm<FinancingVariable>();
    const watchRate = watch('rate', "0")
    const watchDuration = watch('durationMonth', "0")
    const watchApport = watch('apport', "0")
    const watchEcheance = watch('echeance', "0")
    const vat = appConfig.app.defaultVAT
    const currency = appConfig.app.defaultCurrency
    const labEcheance = t("cfg_fin_echeance_"+watchEcheance);

    useEffect(() => {
        const rateInt = Number(watchRate)
        const durationInt = Number(watchDuration)
        const apportInt = Number(watchApport)
        const echeanceInt = Number(watchEcheance)
        const rateRegEx = /^-?[0-9]{0,1}(\.?[0-9]{0,2})?$/

        //const durationRegEx = /^[0-9]{0,5}$/
        const apportRegEx = /^[0-9]{0,9}$/
        rateRegEx.test(watchRate) ? setRateError(false) : setRateError(true)
        //durationRegEx.test(watchDuration) ? setdurationError(false) : setdurationError(true)
        apportRegEx.test(watchApport) ? setapportError(false) : setapportError(true)

        if ( !Number.isNaN(rateInt) && !Number.isNaN(apportInt) && apportInt>0 && !Number.isNaN(echeanceInt) && echeanceInt>0  && !Number.isNaN(durationInt) && durationInt > 0) {
            const priceTTC = calcPriceTTC(totalConfPrice, vat)
           const res = calcMensuality(priceTTC, durationInt,rateInt,apportInt,echeanceInt)
           res && setMensuality(res) 
        } else {
            setMensuality(NaN)
        }

    }, [watchRate, watchDuration,watchApport,watchEcheance])

     

    const onSubmit = (data: FinancingVariable) => {
        console.log(data);
        if (!rateError && !echeanceError ){
            const durationInt = Number(data.durationMonth)
            const rateInt = Number(data.rate)
            const apportInt = Number(data.apport)
            const echeanceInt = Number(data.echeance)
            let financingData: FinancingState =  {
                durationMonth : durationInt,
                rate: rateInt,
                apport: apportInt,
                echeance : echeanceInt
            }
            dispatch(setFinancing(financingData))
            dispatch(hidePopup())
        }
    }



    return (

        <div className="popinfinancement">
            <h5 className="pt-2">{t("cfg_fin_prix_total")} : <span className="price-bold">{formatPrice(calcPriceTTC(totalConfPrice, vat))} chf TTC</span></h5>
            <h5>{t("cfg_fin_prix_echeance")} : <span className="price-bold">{ !Number.isNaN(mensuality) ? formatPrice(mensuality) : '*** '} /{labEcheance}<sup>**</sup> </span></h5>
            <form
                onSubmit={handleSubmit(onSubmit)}
            >
                <div className="w-75 pt-4" >
                    {/* credit period */}
                    <div className="input-group mb-1">
                        <span className="input-group-text">{t("cfg_fin_lab_duree")} : </span>
                        <select
                            id="durationMonth"
                            defaultValue={currentFinancing.durationMonth}
                            className="form-control customSelect"
                            {...register("durationMonth")}
                        >
                            <option value="0">{t("cfg_fin_opt_0")}</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>

                    </div>
                    {/* credit period */}
                    <div className="input-group mb-1 pt-2">
                        <span className="input-group-text">{t("cfg_fin_lab_taux")} : </span>
                        <input 
                            id="rate"
                            defaultValue={currentFinancing.rate} 
                            className="form-control"
                            type="text" 
                            {...register("rate")}
                        />
                        { rateError && 
                            <p className="d-inline-block color-warning text-justify pl-3 py-0 my-0 font-regular">
                                <small>
                                    {`${t("cfg_entrer_nombre")} 1 ${t("cfg_con_et")} 6 ${t("cfg_chiffres")} ${t('cfg_max')} `} 
                                </small>
                            </p>
                        }
                    </div>
                    {/* apport */}
                    <div className="input-group mb-1 pt-2">
                        <span className="input-group-text">{t("cfg_fin_lab_apport")} : </span>
                        <input
                            id="apport"
                            defaultValue={currentFinancing.apport}
                            className="form-control"
                            type="text"
                            {...register("apport")}
                        />
                        { apportError &&
                            <p className="d-inline-block color-warning text-justify pl-3 py-0 my-0 font-regular">
                                <small>
                                    {`${t("cfg_entrer_nombre")} 1 ${t("cfg_con_et")} 6 ${t("cfg_chiffres")} ${t('cfg_max')} `}
                                </small>
                            </p>
                        }
                    </div>
                    {/* type echeance */}
                    <div className="input-group mb-1 pt-2">
                        <span className="input-group-text">{t("cfg_fin_lab_echeance")} : </span>
                        <select
                            id="echeance"
                            defaultValue={currentFinancing.echeance}
                            className="form-control customSelect"
                            {...register("echeance")}
                        >
                            <option value="0">{t("cfg_fin_opt_0")}</option>
                            <option value="1">{t("cfg_fin_opt_1")}</option>
                            <option value="3">{t("cfg_fin_opt_3")}</option>
                            <option value="6">{t("cfg_fin_opt_6")}</option>
                            <option value="12">{t("cfg_fin_opt_12")}</option>
                        </select>
                            { echeanceError &&
                            <p className="d-inline-block color-warning text-justify pl-3 py-0 my-0 font-regular">
                                <small>
                                    {`${t("cfg_entrer_nombre")} 1 ${t("cfg_con_et")} 6 ${t("cfg_chiffres")} ${t('cfg_max')} `}
                                </small>
                            </p>
                        }
                    </div>
                </div>
                
            <div className="row my-2 pt-4">
                <div className="d-flex btnCenter">
                    <CustomBtn
                        label= {t("cfg_val_btn").toUpperCase()}
                        type="submit"
                    />
                </div>
                {/* monthly payment */}
{/*
                <div className="col-6 d-flex justify-content-end align-items-center ">
                    <div>
                        <h5 className="price-mensuality d-inline"> <span>{ !Number.isNaN(mensuality) ? formatPrice(mensuality) : '*** '}  {`/${labEcheance}**`} </span></h5>
                    </div>
                </div>
*/}
            </div>

            </form>
        </div>
    )
}

export default FinancingModal

{
    /*
    Financement
Sur le configurateur
- dans un premier temps, c'est l'agent qui paramètre tout :
- Le système propose une conversion du montant de la machine en échéances.
- Le module s'appelle « Simulateur de financement »
- Affichage du prix total brut dans la pop in
- Au fil de la configuration, l’agent peut intervenir sur le paramétrage du financement en définissant :
              - La durée de 2 à 6 ans, par pas annuels.
              - Le type d’échéance (mensuelle, trimestrielle, semestrielle ou annuelle)
              - Le montant de l’apport.
              - Le taux

Sur les pages
- le montant de l’échéance est affichée à côté du prix total de la configuration.
- Ajout d’une mention à rédiger « CARACTERE ESTIMATIF / AUCUNE VALEUR CONTRACTUELLE OU COMMERCIALE / PAS D'ENGAGEMENT DE L'AGENT OU DE BUCHER »

Sur le générateur d'offre
- Après avoir calculé le montant net de l’équipement à financer, L’agent indique les critères du financement :
durée,
                               type d’échéance
montant de l’apport,
le taux
Il indique si la valeur de reprise vient en apport :
                N.B. : montant net = machine avec ses équipements/options et accessoires complémentaires) ou du prix de la machine en stock dont on soustrait les remises.

- Le système affiche alors le récapitulatif du financement :
Caractéristiques :
- Montant à financer (prix net / CHF ht)

Echéances :
- Montant des échéances
- Nombre d’échéances (mensuelles / trimestrielles /semestrielles / annuelle ) (nombre total – 1)

Montant total à payer :
Ces informations seront complétées par des mentions légales

Sur l’offre
- L’ensemble sera reporté sur l’offre remise au client, (croquis de mise en page en attente de la part de MDC)

     */
}