import { useAppDispatch } from "../../../../../store/hooks/reduxHooks";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { clearEngineSettings } from "../../../../../store/features/engineSetting/engineSettingSlice";
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice";

import CustomBtn from "../../../button/customBtn/CustomBtn";

// styles
import '../modalComponents.scss'
import appConfig from "../../../../../appconfig";
import useAppClearStore from "../../../../../store/hooks/useAppClearStore";



const  CancelConf = () =>  {
  const {t} = useTranslation()
  const navigate = useNavigate() 
  const dispatch = useAppDispatch()
  const clearAppStore = useAppClearStore()
  const gammePageLink = appConfig.pages.gammes.link.abs
  
  const handleConfirm = () => {
    clearAppStore()
    // back index
    navigate(gammePageLink)
  }
  // every time the popup is loaded, show the popup 

  const handleClose = () => {
   dispatch(hidePopup()) 
  }

  return (
    <div>
          <div>
            <p className="my-0 font-regular">{t('cfg_aband_conf_msg')}.</p>
            <p className="my-0 font-regular">{t('cfg_conf_not_save_msg')}.</p>
          </div>

        {/* footer */}
        <div className="d-flex flex-row justify-content-center mt-4 mb-4">
          <CustomBtn 
            label={t("cfg_confirm_btn").toUpperCase()}
            onClick={handleConfirm}
          />
          <CustomBtn 
            label={t("cfg_annul_btn").toUpperCase()}
            onClick={handleClose}
          />
        </div>
    </div>
  );
}

export default CancelConf