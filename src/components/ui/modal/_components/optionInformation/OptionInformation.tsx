import { useEffect, useState } from "react"
import { OptionRowTransformed } from "../../../../../store/features/api/apiSlice"
import { selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import { useAppSelector } from "../../../../../store/hooks/reduxHooks"
import ImagesContainer from "../../../../imagesContainer/ImagesContainer"
import CleanDiv from "../../../div/cleanDiv/CleanDiv"

// style
import '../modalComponents.scss'



type FetchedImagesData  = {
    loading:  | 'pending'| 'succeeded' |'failed', 
    data: {opt: string[]} | string[] 
}

export interface OptionInformationProps {
    option: OptionRowTransformed
}

const OptionInformation = ({option}:OptionInformationProps) => {
    const [images, setImages] = useState<FetchedImagesData>({loading:'pending', data: []})
    const currentLang = useAppSelector(selectCurrentLanguage)
    // dev mode: defined option with images in  public/
    //const optionRef = import.meta.env.PROD ? option.ref : "A0100"
    const optionRef = option.ref

    useEffect(() => {
        fetch(`https://merlo-ch.com/gestion/configurateur/ajax_get_img.php?opt=${optionRef}`)
            .then( res => res.json())
            .then( data => setImages({loading: 'succeeded', data}) )
            .catch(err => {
                setImages({loading: 'failed', data: []})
                console.log('cannot get images')
            })
    }, [])

    return (
        <div>
            {/* images */}
            <div className="my-2">
                { images.loading === 'succeeded' &&  !Array.isArray(images.data) &&  images.data.hasOwnProperty('opt') &&
                       <ImagesContainer
                            images={images.data.opt}
                       /> 
                }
            </div>
            {/* options infos */}
            <CleanDiv 
                className="font-bold mb-1"
                dangerousHTML={option[currentLang].label}
            />
            <CleanDiv 
                className="font-regular"
                dangerousHTML={option[currentLang].comment}
            />
        </div>
    )
}

export default OptionInformation