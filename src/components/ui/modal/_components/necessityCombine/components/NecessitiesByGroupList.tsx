import { useTranslation } from "react-i18next"
import { Language } from "../../../../../../appconfig"
import { fetchedCategoryOptionsRow, OptionRowTransformed } from "../../../../../../store/features/api/apiSlice"
import CleanDiv from "../../../../div/cleanDiv/CleanDiv"

// style
import '../../modalComponents.scss'

export interface NecessitiesByGroup {
   category: fetchedCategoryOptionsRow | undefined 
   options: OptionRowTransformed[]
}

export interface NecessitiesByGroupListProps {
    necessityData: NecessitiesByGroup | undefined 
    lang: Language
}

const NecessitiesByGroupList = ({necessityData, lang}: NecessitiesByGroupListProps) => {
    const {t} = useTranslation()
    return (
        <div className="mb-1 font-italic">
            {/* get group label */}
            {necessityData && necessityData.category &&
               <div>
                <span>{necessityData && necessityData.options && necessityData.options.length > 1 ? "options " : "option "}</span>
                { (necessityData.category[lang] as unknown as string) }
                </div> 
            }
            {/* all options of the group */}
            { necessityData && necessityData.options && necessityData.options.map((option, index)=>
                <div
                    className="font-bold"
                    key={option.ref}
                >
                    <CleanDiv
                        dangerousHTML={option[lang].label+" ("+option.ref+")"}
                    />
                    { index +1 < necessityData.options.length && 
                        <div>{t('cfg_con_ou')}</div>
                    }
                </div>
             )
            }
        </div>
    )
}

export default NecessitiesByGroupList