import React from "react"

import { useTranslation } from "react-i18next"

import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import { fetchedCategoryOptionsRow, OptionRowTransformed, useGetOptionsByModelApiQuery } from "../../../../../store/features/api/apiSlice"
import { addOrRemoveOption, NecessityOptionStatus, selectModel } from "../../../../../store/features/engineSetting/engineSettingSlice"
import { selectCurrentLanguage } from "../../../../../store/features/translate/translateSlice"
import NecessitiesByGroupList, { NecessitiesByGroup } from "./components/NecessitiesByGroupList"

// style
import '../modalComponents.scss'
import CustomBtn from "../../../button/customBtn/CustomBtn"
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice"
import LoadingSpinner from "../../../spinner/LoadingSpinner"
import CleanDiv from "../../../div/cleanDiv/CleanDiv"
export interface NecessityCombineProps {
    necessities: NecessityOptionStatus | undefined
    option: OptionRowTransformed
}

/** show necessities of an option
 *  option can be removed from the list of selected options
 * 
 */
const NecessityCombine = ({ necessities, option}: NecessityCombineProps) => {
    const {t} = useTranslation()
    const model = useAppSelector(selectModel)
    const currentLang = useAppSelector(selectCurrentLanguage)
    const dispatch = useAppDispatch()
    let content: any


    const options = useGetOptionsByModelApiQuery(model!.ref, {
        selectFromResult: ({data, isError, isLoading, isSuccess, isUninitialized}) => ({
            isUninitialized,
            isError,
            isLoading,
            isSuccess,
           /**  get information on an array Of Option  
            *   and on the group of these options
            *   
            *   caution: all options in the array must be in the same group
            * 
            * @param optionsRef  string[] array of option reference 
            * @returns  { category: fetchedCategoryOptionsRow | undefined, options: OptionRowTransformed[]} 
            */ 
            findOptionsByGroup: (optionsRef: string[]): NecessitiesByGroup| undefined => {
                let options: OptionRowTransformed[] = []
                let categoryId: string | undefined
                let category: fetchedCategoryOptionsRow | undefined 
                if (optionsRef.length <= 0) {
                    return undefined 
                }
                optionsRef.forEach(optRef => {
                     const optionFound = data?.list.find(el => el.ref === optRef)
                     if (optionFound) {
                        // get reference of option group
                        if (categoryId === undefined) {
                            categoryId = optionFound.gp_opt
                            try {
                                // get option category label
                                category =  (data?.categoryOptions[categoryId as any]) 

                            } catch (err) {
                                throw err
                            }
                        } 
                        // add option to array
                        options = [...options, optionFound]
                     } 
                })
                return {category, options}
            },
        })
    })

    const handleDelete = () => {
        dispatch(addOrRemoveOption(option))
        dispatch(hidePopup())
    }
    const handleClose = () => {
        dispatch(hidePopup())
    }

    if (options.isError) {
        content = <div>connection error</div>
    } else if(options.isUninitialized) {
        content = <div></div>
    } else if ( options.isLoading){
        content = (
            <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                <LoadingSpinner/>
            </div>
        ) 
    } else if (options.isSuccess) {
        content = (
            <div>
                {/*  selected option */}
                <div className="font-regular">{t('cfg_select_opt_msg')}:</div>
                <CleanDiv
                    className="font-bold"
                    dangerousHTML={option[currentLang].label+" ("+option.ref+")"}
                />
                {/* <div className="my-1 font-regular">{necessities && necessities.count > 1 ? `nécessite les options:`: `nécessite l' option:`}</div> */}
                <div className="my-1 font-regular">{necessities && necessities.count > 1 ? `${t('cfg_necess_options')}:`:  `${t('cfg_necess_option')}:`}</div>

                {/* necessity N */}
                {   necessities?.necessityN && necessities.necessityN.length > 0 &&
                    <>
                    <NecessitiesByGroupList
                        necessityData={options.findOptionsByGroup(necessities.necessityN)}
                        lang={currentLang}
                    />

                    {  necessities?.otherNecessity   && Object.keys(necessities.otherNecessity).length > 0 && 
                        <div className="my-1 font-bold">{t('cfg_con_et')}</div>
                    }
                    </>
                }

                {/* other necessities */}
                {   necessities?.otherNecessity   && Object.keys(necessities.otherNecessity).map((groupRef, index) => 
                    <React.Fragment
                        key={groupRef}
                    >
                        <NecessitiesByGroupList
                            necessityData={options.findOptionsByGroup(necessities.otherNecessity[groupRef])}
                            lang={currentLang}
                        />

                        { index +1 < Object.keys(necessities.otherNecessity).length && 
                            <div>{t('cfg_con_et')}</div>
                        }
                    </React.Fragment>
                )
                }
                {/* <div className="py-1">pour qu'elle puisse équiper votre matériel</div> */}
                <div className="py-1">{t('cfg_necess_equip_materiel')}.</div>


                {/* footer */}
                <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                <CustomBtn 
                    label={t("cfg_suppr_opt").toUpperCase()}
                    onClick={handleDelete}
                />
                <CustomBtn 
                    label={t("cfg_mod_conf").toUpperCase()}
                    onClick={handleClose}
                />
                </div>
            </div>
        )
    }

    return (
        <div>{content}</div>
        
    )
}

export default NecessityCombine