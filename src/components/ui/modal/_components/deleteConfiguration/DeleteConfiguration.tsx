import { useState } from "react"
import { useTranslation } from "react-i18next"
import { getJwtInSessionHelper } from "../../../../../helpers/sessionStoragehelpers"
import { DeleteConfigurationBodyData, useDeleteConfigurationMutation } from "../../../../../store/features/api/apiSlice"
import { clearDeleteConfiguration, selectDeleteConfiguration } from "../../../../../store/features/deleteConfiguration/deleteConfigurationSlice"
import { hidePopup } from "../../../../../store/features/modalpopup/modalPopupSlice"
import { useAppDispatch, useAppSelector } from "../../../../../store/hooks/reduxHooks"
import CustomBtn from "../../../button/customBtn/CustomBtn"
import LoadingSpinner from "../../../spinner/LoadingSpinner"

import '../modalComponents.scss'

const DeleteConfiguration = () => {
   const {t} = useTranslation()
     const [errConf, setErrConf] = useState(false)
     const dispatch = useAppDispatch()
     const [deleteConfTrigger, deleteConfResponse] = useDeleteConfigurationMutation()
     const deleteConfiguration = useAppSelector(selectDeleteConfiguration)
     const jwt = getJwtInSessionHelper()

     const handleConfirm = () => {
         const deleteConfData = deleteConfiguration.configurationData
         if (deleteConfData && jwt ) {
            const bodyData: DeleteConfigurationBodyData = {id_conf:deleteConfData.id_save_conf, jwt}
            deleteConfTrigger(bodyData)
         } else { setErrConf(true)}
     }
     const handleClose = () => {
         dispatch(clearDeleteConfiguration())
         dispatch(hidePopup())
     }
     return (
         <div>
            { deleteConfiguration.configurationData && !deleteConfResponse.isSuccess && 
               <div>
                  <div className="font-regular">{t('cfg_la_config')} <span className="font-bold">{deleteConfiguration.configurationData.name}</span> {t('cfg_etre_suppr')} </div>
               </div>
            }
            {/* delete config success */}
            { deleteConfResponse.isSuccess && 
               <div className="font-regular">{`${t('cfg_la_config')} ${t('cfg_ete_supp')}`}</div>
            }

            {/* delete configuration status */}
            <div className="d-flex m-2 p-2 justify-content-center">
               { /* post configuration state */}
               {  deleteConfResponse.isLoading  &&  
                     <LoadingSpinner/>
               }
               {   (deleteConfResponse.isError || errConf) &&  
                     <div className="font-regular color-warning">{t("cfg_err_suppr_conf_msg")}</div>
               }
            </div>

            {/* footer */}
         { deleteConfResponse.isSuccess 
            ?  <div className="d-flex flex-row justify-content-center mt-4 mb-4">
                  <CustomBtn 
                     label={t("cfg_fermer_btn").toUpperCase()}
                     onClick={handleClose}
                  />
               </div>
            : <div className="d-flex flex-row justify-content-center mt-4 mb-4">
               <CustomBtn 
                  label={t("cfg_confirm_btn").toUpperCase()}
                  onClick={handleConfirm}
               />
               <CustomBtn 
                  label={t("cfg_annul_btn").toUpperCase()}
                  onClick={handleClose}
               />
         </div>

         }
      </div>
     )
}

export default DeleteConfiguration