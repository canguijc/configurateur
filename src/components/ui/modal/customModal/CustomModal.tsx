import { Modal } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { hidePopup } from "../../../../store/features/modalpopup/modalPopupSlice";
import { useAppDispatch, useAppSelector } from "../../../../store/hooks/reduxHooks";
import BtnValid from "../../button/customBtn/CustomBtn";

// styles
import '../modal.scss'

/**
 * id must be unique  if multiple Modal components are present
 */
interface CustomModalProps {
   id: string 
   title?: string
   children?: React.ReactNode
   actionsHandledInChildren?: boolean
   hideButton?: boolean
   size?: 'sm' | 'lg' | 'xl';
   onHide?: () => void 
}

const  CustomModal = ({id, title, children, size, actionsHandledInChildren=false, hideButton=false, onHide}:CustomModalProps) =>  {
  // array of message accepted from popupSlice dispatcher 
  const acceptedType = ['information', 'warning', 'incompatibility', 'combine'] 
  //  

  const {t} = useTranslation()
  const {isVisible, id: modalPopupId } = useAppSelector(state => state.modalPopup)
  const dispatch = useAppDispatch()
  
    // close modal
  const handleHide = () => {
    dispatch(hidePopup()) 
    onHide && onHide()

  }


  // check if  the dispatched type is accepted and can show popup 
  const isTargetedModal = ( id === modalPopupId )

  // control if the popup can be rendered
  if (!isTargetedModal){
    return null
  }

  return (
    <>
      <Modal
        show={isVisible} 
        onHide={handleHide}
        size={size ? size : undefined}
      >
        <Modal.Header>
            <div className="mx-auto">
              {/* only for testing */}
              {title &&
                <h4 className="modal-title">{`${title.toUpperCase()}`}</h4>
              }
            </div>
        </Modal.Header>
        <Modal.Body> 
          {children} 
        </Modal.Body>
        {/*
         footer: can be disabled and  actions on popup will be implemented in children
         */}

        {
          (!actionsHandledInChildren ? !hideButton : hideButton) && 
          <div className="d-flex flex-row justify-content-center mt-2 mb-4">
            <BtnValid 
              label={t("cfg_fermer_btn").toUpperCase()}
              onClick={handleHide}
            />
          </div>
        }
        
      </Modal>
    </>
  );
}

export default CustomModal