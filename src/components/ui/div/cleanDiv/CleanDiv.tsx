import { DetailedHTMLProps, HTMLAttributes } from "react"
import DOMPurify from 'dompurify'
export interface CleanDivProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    dangerousHTML: string
}

const allowedTags = ['br']

const CleanDiv = ({dangerousHTML, ...divProps}: CleanDivProps) => {

    var cleanHTML = DOMPurify.sanitize(dangerousHTML, {ALLOWED_TAGS: allowedTags});
    return (
                <div 
                    {...divProps}
                    dangerouslySetInnerHTML={{__html:cleanHTML}}
                />
        
    )
}

export default CleanDiv