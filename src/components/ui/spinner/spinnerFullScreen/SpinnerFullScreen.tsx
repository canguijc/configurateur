import LoadingSpinner from "../LoadingSpinner"

const SpinnerFullScreen = () => {
    return (
        <div style={{height:"100vh"}}>
            <div className="d-flex justify-content-center align-items-center w-100 h-100 ">
                <LoadingSpinner/>
            </div>
        </div> 
    )
}

export default SpinnerFullScreen