import React, { useEffect } from "react"
import appConfig, { Language, appLanguages } from "../../appconfig"
import { useGetUserJwtMutation } from "../../store/features/api/apiSlice"
import { changeLanguage, selectCurrentLanguage } from "../../store/features/translate/translateSlice"
import { useAppDispatch } from "../../store/hooks/reduxHooks"
import { jwtPayloadDecode, jwtPayloadStringified } from "../../utils/jwtUtils"
import SpinnerFullScreen from "../ui/spinner/spinnerFullScreen/SpinnerFullScreen"

const User = ({children}:{children:React.ReactNode}) => {
    // fetch the jwt
    const [jwtTrigger, responseJwt] = useGetUserJwtMutation()
    const dispatch = useAppDispatch()
    const userKey= appConfig.sessionStorage.userKey
    const loginPage = appConfig.url.loginPage
    let content

    useEffect(() => {
        // no argument needed
        jwtTrigger(null)
    },[])

    if(responseJwt.isUninitialized){
        content = <div></div>
    }
    if(responseJwt.isError) {
        sessionStorage.removeItem(userKey)
        console.log('error retrieving user')
        window.location.href = loginPage

        content = (<div>an error occured during the rendering. </div>)

    } else if ( responseJwt.isLoading) {
        content = (<SpinnerFullScreen/>)
    } else if (responseJwt.isSuccess) {
        // production mode
        if (import.meta.env.PROD) {
            try {
                const  jwtPayload = responseJwt.data
                const jwtParsed = jwtPayloadDecode(jwtPayload) 
                const hasUserAccess= jwtParsed['acces'] === '1' 
                // no jwt send
                if (!jwtPayload || !hasUserAccess ) {
                    sessionStorage.removeItem(userKey)
                    window.location.href = loginPage
                } else {
                    // check if  langue in jwt is valid 
                    // and select the user default language 
                    if( typeof jwtParsed === 'object' &&  
                        jwtParsed.hasOwnProperty('langue') && 
                        typeof jwtParsed['langue'] === 'string' && 
                        appLanguages.some( lang => lang === jwtParsed['langue'].toLocaleLowerCase() ) ) {
                            const userLanguage = jwtParsed['langue'].toLowerCase() as Language
                            dispatch(changeLanguage(userLanguage))
                        } 

                    const userDataStringified = {
                        jwt:jwtPayload,
                        data:jwtPayloadStringified(jwtPayload)
                    }
                    sessionStorage.setItem(userKey, JSON.stringify(userDataStringified))
                    // sessionStorage.setItem(userKey, jwtPayloadStringified(jwtPayload))
                    content = <div>{children}</div>
                } 
            // jwt invalid or error when processing jwt
            } catch (err){
                sessionStorage.removeItem(userKey)
                window.location.href = loginPage
                throw 'user data invalid'
            } 
        // development mode
        } else {
            content = <div>{children}</div>
        }
       }     
    return (<div>{content}</div>)
}

export default User