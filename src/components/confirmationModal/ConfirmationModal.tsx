import { useTranslation } from "react-i18next"
import { useNavigate } from "react-router-dom"
import appConfig from "../../appconfig"
import { clearDeleteConfiguration } from "../../store/features/deleteConfiguration/deleteConfigurationSlice"
import { selectmodalId, showPopup } from "../../store/features/modalpopup/modalPopupSlice"
import { useAppDispatch, useAppSelector } from "../../store/hooks/reduxHooks"
import useAppClearStore from "../../store/hooks/useAppClearStore"
import CustomModal from "../ui/modal/customModal/CustomModal"
import ConfirmAndMakeOffer from "../ui/modal/_components/confirmAndMakeOffer/ConfirmAndMakeOffer"
import DeleteConfiguration from "../ui/modal/_components/deleteConfiguration/DeleteConfiguration"
import RetrieveConfigurationFailed from "../ui/modal/_components/retrieveConfigurationFailed/RetrieveConfigurationFailed"
import RetrieveConfigurationSuccess from "../ui/modal/_components/retrieveConfigurationSucces/RetrieveConfigurationSuccess"

const modalConfirmId = [
    "modalConfigHasBeenSaved", 
    "modalRetrieveConfigurationSuccess", 
    "modalRetrieveConfigurationError",  
    "modalConfirmDeleteConf"
] as const

export type ModalConfirmationDefaultId = typeof modalConfirmId[number] 

const ConfirmationModal = () => {
    const {t} = useTranslation()
    const modalId = useAppSelector(selectmodalId) 
    const dispatch = useAppDispatch()
    const clearAppStore = useAppClearStore()
    const navigate = useNavigate()
    const modalConfigurationHasBeenSavedId: ModalConfirmationDefaultId  = "modalConfigHasBeenSaved" 
    const modalRetrieveConfigurationSuccessId : ModalConfirmationDefaultId  = "modalRetrieveConfigurationSuccess" 
    const modalRetrieveErrorId: ModalConfirmationDefaultId = "modalRetrieveConfigurationError"
    const modalConfirmDeleteConfigurationId: ModalConfirmationDefaultId = "modalConfirmDeleteConf"
    const gammesPagePath = appConfig.pages.gammes.link.abs
    const savedConfigModalId = "savedConfigModalId"

    const handleRetrieveConfModalHide = () => {
        clearAppStore()
        navigate(gammesPagePath)
        dispatch(showPopup(savedConfigModalId))
    }

    const handleConfigSavedHide = () => {
        clearAppStore()
        navigate(gammesPagePath)
    }

    const handleDeleteConfModalHide = () => {
        dispatch(clearDeleteConfiguration())
    }

    //  confirmation Modal after saving and make offer 
    if (modalId === modalConfigurationHasBeenSavedId) {
        return (
            <CustomModal
                id={modalConfigurationHasBeenSavedId}
                title={t("cfg_suppr_conf")}
                actionsHandledInChildren={true}
                onHide={handleConfigSavedHide}
            >
                <ConfirmAndMakeOffer/>
            </CustomModal>
        )    
    } 

    //  Modal  success retrieving configuration 
    if (modalId === modalRetrieveConfigurationSuccessId) {
        return (
            <CustomModal
                id={modalRetrieveConfigurationSuccessId}
                title={t('cfg_reprise_conf')}
            >
                <RetrieveConfigurationSuccess
                    text={t("cfg_reprise_succes_msg")}
                />
            </CustomModal>
        )    
    }

    //  Modal  error retrieving configuration 
    if (modalId === modalRetrieveErrorId) {
        return (
            <CustomModal
                id={modalRetrieveErrorId}
                title={t('cfg_reprise_conf')}
                onHide={handleRetrieveConfModalHide}
            >
                <RetrieveConfigurationFailed/>
            </CustomModal>
        )
    }

    //  Modal  confirm DeleteConfirmation 
    if (modalId === modalConfirmDeleteConfigurationId) {
        return (
            <CustomModal
                id={modalConfirmDeleteConfigurationId}
                title={t('cfg_suppr_conf')}
                onHide={handleDeleteConfModalHide}
                actionsHandledInChildren={true}
            >
                <DeleteConfiguration/>
            </CustomModal>
        )
    }
    // no right id specified
    return null
}

export default ConfirmationModal