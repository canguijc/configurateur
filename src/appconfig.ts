/**
 *  configuration file of the App constants
 */

// is one value of languages "fr" | "de" and readonly
export type Language =  typeof   appLanguages[number] 
// is  languages ["fr", "de"] and readonly
export type Languages = typeof appLanguages

// here we can define other accepted translation languages

export type AppPageIndex = typeof appPagesIndex[number]
export type AppPagesIndex = typeof appPagesIndex
export type PagesConfig = {
    [key in AppPageIndex]: {
        link: {
            rel: string
            abs: string
        } 
        title?: string
        step?: string
    }
}

// languages accepted in app
export const appLanguages =["fr", "de"]  as const 
//  pages of app 
export const appPagesIndex = ["gammes", "config", "models", "options", "resume", "retrieveConfig"] as const

// config of all app pages
export const appPages: PagesConfig =  {
       gammes: { link: {abs:"/", rel: "/"}, title:"cfg_sel_gamme", step:"1"  },
       config: { link: {abs: "/config", rel: "config"}, title:"cfg_sel_gamme" },
       models: { link: {abs: "/config/models", rel: "models"}, title:"cfg_sel_modele", step:"2" },
       options: { link: {abs: "/config/options", rel: "options"}, title:"cfg_sel_option", step:"3"  },
       resume: { link: {abs: "/resume", rel: "resume"}, title: "cfg_resume_conf", step: "4"  },
       retrieveConfig: { link: {abs: "/testRetrieveConfig", rel: "testRetrieveConfig"} },
    //    RetrieveConfig: { link: {abs: "/testRetrieveConfig", rel: "testRetrieveConfig"}, title: " test retrieveConfig", step: "5"  },
}
const prodEnv = import.meta.env.PROD
const baseApiPath= import.meta.env.VITE_API_BASE_PATH
const routerBasePath = import.meta.env.VITE_ROUTER_BASE_PATH

export const appConfig = 
{

    app: {
        defaultCurrency:'chf',
        defaultVAT: 7.7 
    },
    url: {
        loginPage: `${baseApiPath}/security/login.php`,
        indexPage: `${baseApiPath}/`,
        imagesDirUrl: prodEnv ? `${routerBasePath}public` : `${baseApiPath}/configurateur/public` ,
    },
    translation: {
        // array of accepted languages
        languages: appLanguages,
        // must be a  value of languages array
        defaultLanguage: "fr" as Language,
        baseTranslationUrl: `${baseApiPath}/api/api.php`
    },
    api: {
        baseUrl: `${baseApiPath}`,

        jwtUrl: `${baseApiPath}/api/api.php?user=9`,
        // not working on dev.merlo
        baseModelSpecUrl: `https://merlo-ch.com/api/api.php?spec=1&model=`,
        saveConfigurationUrl:`${baseApiPath}/api/api.php`,
        makeOfferUrl:`${baseApiPath}/configurateur_offre?para1=`,
        getModelsPath: `gestion/configurateur/ajax_get_machines.php?gamme=`,
        getOptionsPath:`gestion/configurateur/ajax_get_options_modele.php?reference=`,
        savedConfigurationByUserPath:`api/api.php?configurateurSave=1&id_user=`,
    },
    sessionStorage:  {
        userKey: "user"
    },
    pages: appPages 
}

export default appConfig
