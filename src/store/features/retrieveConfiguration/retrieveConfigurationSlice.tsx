import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../rootReducer";
import { SaveConfigurationData, SavedConfigDataItem } from "../api/apiSlice";

export type  RetrieveConfigurationStatus = 'uninitialized' | 'failed' | 'initRetrieving' | 'canCheckGamme' | 'canCheckModel' | 'canCheckOptions' | 'succeeded' 

export type  RetrieveConfiguration = {
    configurationData: SavedConfigDataItem  | undefined
    error: {
    hasError?: boolean
    optionsNotInList?: string[]
    optionsWithIncompatibility?: string[]
    msg?: string
    } 
    status: RetrieveConfigurationStatus
}

const initialState = {
    error: {},
    status: 'uninitialized'
} as RetrieveConfiguration 

export const retrieveConfigurationSlice = createSlice({
    name: 'retrieveConfiguration',
    initialState,
    reducers: {
      // Retrieve saved configuration: set options error 
      setRetrieveConfOptionsNotInList: (state, action: PayloadAction<{list: string[] | undefined, msg?: string}>)=> {
        //  if  some options from saved configuration  are missing
        if( Array.isArray(action.payload.list) && action.payload.list.length > 0) {
          state.error.hasError = true
          action.payload.msg && (state.error.msg = action.payload.msg)
          state.error.optionsNotInList = action.payload.list
        }
      },
      setRetrieveError: (state, action: PayloadAction<string>) => {
        if (typeof action.payload === 'string'){
          state.error.msg = action.payload
          state.error.hasError = true
        }
      },
      // Retrieve saved configuration: set options  with incompatibility error 
      setRetrieveConfOptionsWithIncompatibility: (state, action: PayloadAction<{list: string[] | undefined, msg?: string}>) => {
        if( Array.isArray(action.payload.list) && action.payload.list.length > 0) {
          state.error.hasError = true
          action.payload.msg && (state.error.msg = action.payload.msg)
          state.error.optionsWithIncompatibility = action.payload.list
        }
      },
      setRetrieveConfigurationData: (state, action: PayloadAction<SavedConfigDataItem>) => {
        state.configurationData = action.payload
        state.status = 'initRetrieving'
      },
      // set state of the retrieving configuration step
      setRetrieveConfigurationStatus: (state, action: PayloadAction<RetrieveConfigurationStatus>) => {
        state.status = action.payload
      },
      clearRetrieveConfiguration: (state) => state = initialState ,
      clearRetrieveStatus: (state) => {state.status = initialState.status}
    },

})

export const {
  setRetrieveConfOptionsNotInList,
  setRetrieveConfOptionsWithIncompatibility,
  setRetrieveConfigurationStatus,
  setRetrieveError,
  setRetrieveConfigurationData,
  clearRetrieveConfiguration,
  clearRetrieveStatus
} = retrieveConfigurationSlice.actions

export const selectRetrieveConfiguration = (state: RootState) => state.retrieveConfiguration
export const selectRetrieveStatus = (state: RootState)  => state.retrieveConfiguration.status
export const selectRetrieveErrors = (state: RootState)  => state.retrieveConfiguration.error
export const selectRetrieveErrorMessage = (state: RootState)  => state.retrieveConfiguration.error.msg
export const selectRetrieveConfigurationData = (state: RootState) => state.retrieveConfiguration.configurationData
export const selectRetrieveConfigurationOptionsRef = (state: RootState) => state.retrieveConfiguration.configurationData?.conf.options
export default retrieveConfigurationSlice.reducer