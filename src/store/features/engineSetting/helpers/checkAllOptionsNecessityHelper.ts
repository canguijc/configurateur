import { OptionRowTransformed } from "../../api/apiSlice"
import { NecessityOptionStatus } from "../engineSettingSlice"
import { checkOptionNecessityHelper } from "./checkOptionNecessityHelper"

/** return an array of necessities for each option in optionList
 *   
 * @param optionList list of options to check 
 * @param optionRefList  
 * @returns 
 */
export const checkAllOptionsNecessityHelper = (optionList: OptionRowTransformed[]) => {
    // list of options reference
    const optionRefList: string[] = optionList.map(option => option.ref) 
    let newNecessityOptionStatus: NecessityOptionStatus[] = [] 
    optionList.forEach(el => {
        const necessityOption  = checkOptionNecessityHelper(el,optionRefList) 
        if (necessityOption && necessityOption.count > 0) {
        newNecessityOptionStatus =  [...newNecessityOptionStatus, necessityOption ]
        }
    })
    return newNecessityOptionStatus
}