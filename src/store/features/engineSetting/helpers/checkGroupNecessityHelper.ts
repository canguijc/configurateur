import { OptionRowTransformed } from "../../api/apiSlice"
import { NecessityOptionStatus } from "../engineSettingSlice"

/** list groups of options that have some necessity not fullfilled
 * 
 * @param necessityOptionArray array of options that need necessity 
 * @param optionsList array of options 
 * @returns array of group reference 
 */
export const checkGroupNecessityHelper = (necessityOptionArray: NecessityOptionStatus[], optionsList: OptionRowTransformed[]): string[] => {
          // reference of options that need necessities
          const  optionNecessityRef =  necessityOptionArray.map(el => el.ref)
          const optionListNeedNecessity = optionsList.filter(el => optionNecessityRef.includes(el.ref) ) 
          // group reference list  that need a necessity
          const optionGroupRef = [... new Set(optionListNeedNecessity.map(el => el.gp_opt ))]
          return optionGroupRef
}