// filtering option first

import { arrayIntersectUtils } from "../../../../utils/arrayUtils"
import { OptionRowTransformed } from "../../api/apiSlice"

/** get an array of incompatibility reference that can be removed  
 *  if one of optionToRemove constraint exist in OptionArray element, 
 *  it will not be present in return array 
 * 
 * @param OptionArray  array of selected options
 * @param optionToRemove  option to remove
 * @returns  string[] contains incompatibility reference that can be removed 
 */
export const allowedIncompatibilitiesToRemoveHelper = (OptionArray: OptionRowTransformed[], optionToRemove: OptionRowTransformed) => {
    // no incompatibility dependency
    if (!optionToRemove.constraint || !optionToRemove.constraint.incomp) return [] 

    // keep references of incompatibility in common
    let dependantIncompArray: string[] = []
    // compare incompatibility dependency
    OptionArray.forEach(el => {
        // discard option to remove from  options array 
        if (el.ref == optionToRemove.ref ) return  
        // if no constraint on current element or on optionToRemove, skip this iteration 
        if (!el.constraint || !el.constraint.incomp  ||  !optionToRemove.constraint || !optionToRemove.constraint.incomp) return 
        else {
            const commonIncomp =  arrayIntersectUtils(el.constraint.incomp, optionToRemove.constraint.incomp )
            dependantIncompArray = [...dependantIncompArray, ...commonIncomp]
        }
    })
    // incompatibilities that can be remove
    const incompatToRemoveArray = optionToRemove.constraint.incomp.filter(el => ![... new Set(dependantIncompArray)].includes(el))
    return incompatToRemoveArray 
}