import { arrayIntersectUtils } from "../../../../utils/arrayUtils"
import { NecessityConstraint, OptionRowTransformed } from "../../api/apiSlice"
import { NecessityOptionStatus } from "../engineSettingSlice"

/**
 * 
 * @param option  option to check is necessity dependency
 * @param optionsSelected  array of option ref  
 * @returns  a NecessityOptionStatus object if necessities of option aren't fullfilled.  undefined if no necessities needed
 */
export const checkOptionNecessityHelper = (option: OptionRowTransformed, optionsSelected: string[]) => {
        const necessityStatus: NecessityOptionStatus = {necessityN: [], otherNecessity: {}, ref: '', count: 0}
        let count = 0

        if(option.constraint?.necessite && option.constraint.necessite){
            const necessityKeys = Object.keys(option.constraint.necessite).length
            let otherNecessityNotFullFiled: NecessityConstraint = {} 
            if (necessityKeys > 0){
                // necessity N
                const necessityNKey: any = "N"
                const hasNecessityN = option.constraint.necessite.hasOwnProperty(necessityNKey)
                if (hasNecessityN){
                    const necessityN = option.constraint.necessite[necessityNKey] as unknown as string[]
                    const isNecessityNFilled = arrayIntersectUtils(necessityN, optionsSelected).length > 0
                    if(!isNecessityNFilled) {
                        // fill the necessity N needed
                        necessityStatus.necessityN = necessityN
                        count++
                    } else {
                        necessityStatus.necessityN = []
                    } 
                } 

                // other necessity N1 N2 N3 ...
                Object.keys(option!.constraint!.necessite!).forEach((necessityKey) => {
                    // skip this iteration
                    if (necessityKey == necessityNKey) return ;
                    // process other necessities
                    const isOtherNecessityNFullfilled = arrayIntersectUtils((option!.constraint!.necessite![necessityKey] as unknown as string[]), optionsSelected).length > 0
                    if (!isOtherNecessityNFullfilled) {
                        otherNecessityNotFullFiled[necessityKey] = option!.constraint!.necessite![necessityKey] 
                        count++
                    }
                })

                necessityStatus.otherNecessity = otherNecessityNotFullFiled 
                necessityStatus.ref = option.ref
                necessityStatus.count= count
                return necessityStatus
               
            } 
        }
}