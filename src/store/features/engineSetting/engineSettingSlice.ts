import { createSlice, current } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


import  { Gamme } from '../gamme/gammesData'
import { Model, OptionRowTransformed, NecessityConstraint} from '../api/apiSlice'
import { RootState } from '../../rootReducer'
import { arrayConcatWithoutDuplicateUtils  } from '../../../utils/arrayUtils'
import { allowedIncompatibilitiesToRemoveHelper } from './helpers/allowedIncompatibilitiesToRemoveHelper'
import { checkAllOptionsNecessityHelper } from './helpers/checkAllOptionsNecessityHelper'
import { checkGroupNecessityHelper } from './helpers/checkGroupNecessityHelper'


export type NecessityOptionStatus = {
      ref: string
      necessityN: string[]
      otherNecessity: NecessityConstraint
      count: number
}

export type OptionsStatus = {
  selectedOptionsRef: string[]
  incompatibility: string[] 
  necessity: NecessityOptionStatus[]
  groupNecessity: string[]
}

//  type accepted in EngineSetting state
export type EngineSettingState = {
  gamme?: Gamme 
  model?: Model
  testMode: boolean 
  options: {
    list: OptionRowTransformed[]
    lastSelectedOption?: OptionRowTransformed 
    optionsStatus:  OptionsStatus  
  }
  total: {
    totalOptionsPrice: number
    totalConfigurationPrice: number
  }
}

// Define the initial state using that type
const initialState: EngineSettingState = {
  gamme: undefined,
  model: undefined,
  testMode: false,
  options: {
    list: [],
    optionsStatus: {
      selectedOptionsRef: [],
      incompatibility: [],
      necessity: [],
      groupNecessity: []
    } 
  },
  total: {
    totalOptionsPrice: 0,
    totalConfigurationPrice: 0
  }
}

export const engineSettingSlice = createSlice({
  name: 'engineSettings',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // update the selected gamme
    setGamme: (state, action: PayloadAction<Gamme>) => {
        state.gamme = action.payload 
    },
    setModel: (state, action: PayloadAction<Model>) => {
      state.model = action.payload 
      state.total.totalConfigurationPrice = Math.ceil(action.payload.prix)
    },
    // reset  Model and Option state when a Model is removed
    clearModel : (state) => {
      state.model = undefined
      state.options = initialState.options 
      state.total = initialState.total 
    },
    // add an option to selected array
    addOrRemoveOption: (state, action: PayloadAction<OptionRowTransformed>) => {
        const isOptionInArray = state.options.list.find(el => el.ref == action.payload.ref)
        const constraint = action.payload.constraint

        // if option is in array remove it  
        if (isOptionInArray) {
        // filtering option first
          // incompatibility  that  no selected option depend of   
          const incompatToRemove = allowedIncompatibilitiesToRemoveHelper(state.options.list, action.payload)
          // remove the incompatibilities
          state.options.optionsStatus.incompatibility = state.options.optionsStatus.incompatibility.filter(el => !incompatToRemove.includes(el) )


          // remove reference of option in selectedOptionRef array too.
          state.options.optionsStatus.selectedOptionsRef = state.options.optionsStatus.selectedOptionsRef.filter(el => el !== action.payload.ref)
          state.options.list = state.options.list.filter(el => el.ref !== action.payload.ref ) 
          // delete the  last selected option
          state.options.lastSelectedOption = undefined 

          // process necessity
          // check if other selected options have necessity dependency of this option 
          // new array of necessity for each option
          const newNecessityOptionArray = checkAllOptionsNecessityHelper([...state.options.list])
          state.options.optionsStatus.necessity = newNecessityOptionArray

          // group reference list  that need a necessity
          state.options.optionsStatus.groupNecessity = checkGroupNecessityHelper(newNecessityOptionArray, [...state.options.list])

          // else add option   
        } else {
          // filtering option first
          // adding incompatibility to optionsStatus
          if(constraint  && constraint.incomp){
              state.options.optionsStatus.incompatibility = arrayConcatWithoutDuplicateUtils(state.options.optionsStatus.incompatibility, constraint.incomp)
          }

          // add reference of option in selectedOptionRef array too.
          state.options.optionsStatus.selectedOptionsRef = [...state.options.optionsStatus.selectedOptionsRef, action.payload.ref]
          // add new option to the list of selected options
          state.options.list = [...state.options.list, action.payload]
          // set the last option selected 
          state.options.lastSelectedOption = action.payload

          // process necessities
          // get new array of necessity
          const newNecessityOptionArray = checkAllOptionsNecessityHelper([...state.options.list]) 
          state.options.optionsStatus.necessity = newNecessityOptionArray
          // group reference list  that need a necessity
          state.options.optionsStatus.groupNecessity = checkGroupNecessityHelper(newNecessityOptionArray, [...state.options.list])

        }
         // sum the selected options price
         const optionsSum = state.options.list
            .reduce((prev, next) => {
               return (prev + Math.ceil(next.prix))
            }, 0)
          // calculate the configuration price
          state.total.totalOptionsPrice = optionsSum
          state.total.totalConfigurationPrice = state.model ? state.model.prix + optionsSum  : NaN
    },
    // setSaveConfigurationId: (state, action: PayloadAction<string>) => {
    //     if  (typeof action.payload === 'string') {
    //       state.status.saveConfiguration.id = action.payload
    //     }
    // },
    // clearSaveConfiguration: (state) => {
    //   state.status.saveConfiguration = initialState.status.saveConfiguration
    // },
    // reset  to initial state 
    clearEngineSettings : (state) => state = initialState,
    clearOptions: (state)  => { 
      state.options = initialState.options 
      state.total.totalOptionsPrice = 0
      state.total.totalConfigurationPrice = state.model ? state.model?.prix : 0
    },
    clearLastOptionSelected: (state) => {
      state.options.lastSelectedOption = undefined
    },
    setTestMode: (state, action: PayloadAction<number>) => {
        if ( typeof action.payload === 'number' && action.payload === 1 ) {
          state.testMode = true 
        } else {
          state.testMode = false 
        }
    }
  },
})

// actions on gamme
export const {
  setGamme,
  setModel,
  // setSaveConfigurationId,
  setTestMode,
  addOrRemoveOption,
  clearEngineSettings,
  clearModel,
  clearOptions,
  clearLastOptionSelected,
  // clearSaveConfiguration,
 } = engineSettingSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const selectGamme = (state: RootState) => state.engineSettings.gamme
export const selectModel = (state: RootState) => state.engineSettings.model
export const selectOptions = (state: RootState) => state.engineSettings.options.list
export const selectLastOption = (state: RootState) => state.engineSettings.options.lastSelectedOption
export const selectOptionsRef = (state: RootState) => state.engineSettings.options.optionsStatus.selectedOptionsRef
export const selectTotalOptionsPrice = (state: RootState) => state.engineSettings.total.totalOptionsPrice
export const selectTotalConfigurationPrice = (state: RootState) => state.engineSettings.total.totalConfigurationPrice
export const selectOptionStatus = (state: RootState) => state.engineSettings.options.optionsStatus
export const selectOptionStatusIncompatibility = (state: RootState) => state.engineSettings.options.optionsStatus.incompatibility
export const selectOptionStatusNecessity = (state: RootState) => state.engineSettings.options.optionsStatus.necessity
export const selectOptionStatusGroupNecessity = (state: RootState) => state.engineSettings.options.optionsStatus.groupNecessity
export const selectTestMode = (state: RootState) => state.engineSettings.testMode
// export const selectSaveConfiguration = (state: RootState) => state.engineSettings.status.saveConfiguration
export const selectconfiguration = (state:RootState) => ({
    gamme: state.engineSettings.gamme,
    model: state.engineSettings.model,
    options: state.engineSettings.options,
    total: state.engineSettings.total,
  }) 


// c' est le reducer a importer dans le store
export default engineSettingSlice.reducer