
import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState } from "../../rootReducer"
import { SavedConfigDataItem } from "../api/apiSlice"


type DeleteConfigurationStatus = 'uninitialized' | 'failed' | 'succeeded'
export interface DeleteConfigurationState {
    configurationData: SavedConfigDataItem  | undefined
    status: DeleteConfigurationStatus 
}

const initialState: DeleteConfigurationState = {
      configurationData: undefined,
      status: 'uninitialized'
}



export const deleteConfigurationSlice = createSlice({
  name: 'deleteConfiguration',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setDeleteConfigurationData: (state, action: PayloadAction<SavedConfigDataItem>) => {
          state.configurationData = action.payload
    },
    setDeleteConfigurationStatus: (state, action: PayloadAction<DeleteConfigurationStatus>) => {
        state.status = action.payload
    },
    clearDeleteConfiguration: (state) => state = initialState 
    },
})

export const {
    setDeleteConfigurationData,
    setDeleteConfigurationStatus,
    clearDeleteConfiguration
} = deleteConfigurationSlice.actions

export const selectDeleteConfiguration = (state: RootState) => state.deleteConfiguration

export default deleteConfigurationSlice.reducer