import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

import { RootState } from '../../rootReducer'

import gammesData, { Gamme } from './gammesData'

// Define a type for the slice state

export interface GammeState {
  gammes: Gamme[]
  currentGamme?: Gamme 
  error: boolean
}
// Define the initial state using that type
const initialState: GammeState = {
    gammes: gammesData,
    currentGamme: undefined,
    error: false
}

export const gammeSlice = createSlice({
  name: 'gamme',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    clearGamme: (state) => state = initialState
  },
})

// actions on gamme
export const {
  clearGamme
} = gammeSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const getAllGammes = (state: RootState) => state.gamme.gammes


// c' est le reducer a importer dans le store
export default gammeSlice.reducer