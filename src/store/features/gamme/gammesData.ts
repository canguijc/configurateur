
import cingo from '../../../assets/img/models/cingo.png'
import compact from '../../../assets/img/models/compact.png'
import dbm from '../../../assets/img/models/dbm.png'
import electrique from '../../../assets/img/models/electrique.png'
import multifarmer from '../../../assets/img/models/multifarmer.png'
import panoramic_HC from '../../../assets/img/models/panoramic_HC.png'
import panoramic_stab from '../../../assets/img/models/panoramic_stab.png'
import roto from '../../../assets/img/models/roto.png'
import treemme from '../../../assets/img/models/treemme.png'
import turbofarmer_HC from '../../../assets/img/models/turbofarmer_HC.png'
import turbofarmer_MC from '../../../assets/img/models/turbofarmer_MC.png'

export interface Gamme {
     gamme: string 
     image: string
     alt: string
     reference: string
}

  const gammesData: Gamme[] =[
  { gamme: "Electrique", reference: "ELE", image: electrique, alt: "electrique"},
  { gamme: "Compact", reference: "COM", image: compact, alt: "compact"},
  { gamme: "Turbofarmer Medium Capacity", reference: "TMC", image: turbofarmer_MC, alt: "Turbofarmer Medium Capacity"},
  { gamme: "Turbofarmer High Capacity", reference: "THC", image: turbofarmer_HC, alt: "Turbofarmer High Capacity"},
  { gamme: "Panoramic Stabilisateur", reference: "PST", image: panoramic_stab, alt: "Panoramic"},
  { gamme: "Panoramic High Capacity", reference: "PHC", image: panoramic_HC, alt: "Panoramic High Capacity"},
  { gamme: "Multifarmer", reference: "MUL", image: multifarmer, alt: "Multifarmer"},
  { gamme: "Roto", reference: "ROT", image: roto, alt: "Roto"},
  { gamme: "DBM", reference: "DBM", image: dbm, alt:"DBM"},
  { gamme: "Cingo", reference: "CIN", image: cingo, alt: "Roto"},
  { gamme: "Treemme", reference: "TRE", image: treemme, alt:"DBM"},
]

export default gammesData;