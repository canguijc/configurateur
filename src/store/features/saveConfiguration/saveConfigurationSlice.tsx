import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState } from "../../rootReducer"


type SaveConfigurationStatus = {
      id: string | undefined
      name: string | undefined
}
export interface SaveConfigurationState {
    status: SaveConfigurationStatus 
}

const initialState: SaveConfigurationState = {
  status: {
      id: undefined,
      name: undefined
  }
}



export const saveConfigurationSlice = createSlice({
  name: 'saveConfiguration',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setSaveConfigurationId: (state, action: PayloadAction<string>) => {
        if  (typeof action.payload === 'string') {
          state.status.id = action.payload
        }
    },
    setSaveConfigurationName: (state, action: PayloadAction<string>) => {
        if  (typeof action.payload === 'string') {
          state.status.name = action.payload
        }
    },
    setSaveConfigurationStatus: (state, action: PayloadAction<SaveConfigurationStatus>) => {
        state.status = action.payload
    },
    clearSaveConfiguration: (state) => state = initialState 
    },
})

export const {
    setSaveConfigurationId,
    setSaveConfigurationName,
    setSaveConfigurationStatus,
    clearSaveConfiguration
} = saveConfigurationSlice.actions

export const selectSaveConfiguration = (state: RootState) => state.saveConfiguration.status

export default saveConfigurationSlice.reducer