import { createSlice, current, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../rootReducer";

export type FinancingState = {
    rate: number
    durationMonth: number
    apport : number
    echeance : number
}

const initialState: FinancingState = {
    rate: 0,
    durationMonth: 0,
    apport : 0,
    echeance : 0
} 

const financingSlice = createSlice({
    name:'financing',
    initialState: initialState,
    reducers: {
        setFinancing: (state, action: PayloadAction<FinancingState>) => {
            state.durationMonth = action.payload.durationMonth
            state.rate = action.payload.rate
            state.apport = action.payload.apport
            state.echeance = action.payload.echeance
        }, 
        clearFinancing: (state) => state = initialState  
    }
})

// actions
export const {setFinancing, clearFinancing} = financingSlice.actions

// select
export const selectFinancing = (state: RootState) => state.financing

export default financingSlice.reducer 