// Need to use the React-specific entry point to allow generating React hooks
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import appConfig, { Language, Languages } from '../../../appconfig'
import { arrayIntersectUtils } from '../../../utils/arrayUtils'

/* ----- User jwt Api types ----- */
type User = {
  nom: string
  langue: string
  role: string
}

type TestMod = {
  tmp?: number
}
/* ----- model Api types ----- */
// data by model reference
type ModelData =  {
  label: string, 
  prix: number
}
// fetched data structure of models

type FetchedModelsData = TestMod & {
  [key: string]: ModelData 
}

// type used in app after transforming
export type Model =  ModelData & {
  ref: string
}
export type Models = {
  models: Model[],
  tmp?: number
}


/* ----- option Api types ----- */

// translation of each options category
type OptionLang = { [key in Language]: string }

// ranking of the options categories
export type  CategoryRank = string[]

// type of each option data
type OptionDataLang = {[key in Language]: {
    label: string 
    comment: string
    alert: string
}}

// type  from each option row 
type OptionRowData = OptionDataLang &  {
    prix: number
    gp_opt: string
}


// type of the fetched data for the category label from the endpoint 
export type fetchedCategoryOptionsRow = {
  [key: string] : (OptionLang & { rang: string}) 
}

export type NecessityConstraint = { [key: string]: string[]}
export type IncompatibilityConstraint = string[]
export type Constraint = {
    necessite?: NecessityConstraint 
    incomp?:  IncompatibilityConstraint 
}

// types for the incompatibilityTable 
type IncompatibilityTableData = {
  [key:string] : Constraint 
}


// fetched Data structure of options
type FetchedOptionData = {
  categoryRank: CategoryRank 
  categoryOptions: fetchedCategoryOptionsRow[]
  incompatibilityTable: IncompatibilityTableData 
  tmp?: number 
  options: {
    [key: string]: OptionRowData
  }
}
//type used in app after transforming
export type OptionRowTransformed = OptionRowData & {
    constraint?: Constraint 
    ref: string
}

// type used in App after transforming
export type OptionsData =  {
    categoryRank: CategoryRank 
    categoryOptions: fetchedCategoryOptionsRow[],
    tmp?: number
    // incompatibilityTable:IncompatibilityTableData
    list: OptionRowTransformed[] 
}

export type ConfigurationItems = {
    gamme: {
      label: string
      ref: string
    }
    model: {
      label: string
      ref: string
      prix?: number
    },
    options: string[] 
    total: {
      totalOptionsPrice: number
      totalConfigurationPrice: number
    }
}

// body send when saving a configuration
export type SaveConfigurationBodyData = {
  configuration: ConfigurationItems  
  name: string
  jwt?: string
}

export type UpdateConfigurationBodyData = SaveConfigurationBodyData & {
  id_conf?: string
}

export type DeleteConfigurationBodyData =  {
  jwt?: string
  id_conf?: string
}

// response returned when saving configuration
export type  ConfigurationResult = {
  id_conf?: string
}

// retrieved saved configuration data info
export type SaveConfigurationData = {
  id_save_conf: string
  id_user: string
  name: string
    dateCreAt:string
    agent:string
    nom:string
    prenom:string
}
// fetched data
export type FetchedSavedConfigDataItem = SaveConfigurationData & {
  data: string
    dateCreate:string
    agent:string
    nom:string
    prenom:string
  // data: ConfigurationItems
}


export type SavedConfigDataItem = {
  name: string
  conf: ConfigurationItems
  id_save_conf: string
  id_user: string
    dateCreAt:string
    agent:string
    nom:string
    prenom:string
}
// type used in app
export type SavedConfigurations = SavedConfigDataItem[]

/**
 * Define a service using a base URL and expected endpoints
 */
const apiSlice = createApi({
  reducerPath: 'engineApi',
  baseQuery: fetchBaseQuery({ baseUrl: appConfig.api.baseUrl}),
  endpoints: (builder) => ({
    getUserJwt: builder.mutation({
        query:() => ({
          url:`${appConfig.api.jwtUrl}`,
          method: 'GET'
        }),
    }),
    //make an offer
    makeOffer: builder.mutation({
      query: (offerConfigData) => ({
        url: `${appConfig.api.makeOfferUrl}`,
        method: 'POST',
        body: offerConfigData ? offerConfigData : {}
      })
     }),
    // save the configuration
    saveConfiguration: builder.mutation<ConfigurationResult,SaveConfigurationBodyData>({
      query: (savedConfigData: SaveConfigurationBodyData) => ({
        url: `${appConfig.api.saveConfigurationUrl}`,
        method: 'POST',
        body: savedConfigData ? savedConfigData : {}
      })
     }),
    // update the configuration
    updateConfiguration: builder.mutation<ConfigurationResult,UpdateConfigurationBodyData>({
      query: (updatedConfigData: UpdateConfigurationBodyData) => ({
        url: `${appConfig.api.saveConfigurationUrl}`,
        method: 'PUT',
        body: updatedConfigData ? updatedConfigData : {}
      })
     }),
     // delete a configuration
    deleteConfiguration: builder.mutation<ConfigurationResult,DeleteConfigurationBodyData>({
      query: (deleteConfigData: DeleteConfigurationBodyData) => ({
        url: `${appConfig.api.saveConfigurationUrl}`,
        method: 'DELETE',
        body: deleteConfigData ? deleteConfigData : {}
      })
     }),
    /**
     * fetching models by gamme reference
     */
    getAllModelApi: builder.query<Models, string>({
      // ajax_get_machines.php?gamme=COM
      query: (ref) => `${appConfig.api.getModelsPath}${ref}`,
      // res is the data fetched and we transform it to be of ModelsData[] type
      transformResponse: ((res: FetchedModelsData, _meta, _arg) => {
        let modelKeys = Object.keys(res) as string[]
        let tmp: number | undefined = undefined 
        let result : Model[] = [] as Model[]
         modelKeys.forEach(key => {
          if(key !== 'tmp'){
            result = [ ...result, { ref: key, ...res[key]}]
          } else {
            tmp = res[key]
          } 
        })
        // result is of ModelsData[] type
        return { tmp: res.tmp, models:result}
      }),
    }),
    /**
     * fetching options by model reference and group
     */
    getOptionsByModelApi: builder.query<OptionsData, string>({
      // ajax_get_options.php?reference=MA00909&groupe=8
      query: (modelRef) => `${appConfig.api.getOptionsPath}${modelRef}`,
      // res is the data fetched and we transform it to be of ModelsData[] type
      transformResponse: ((res: FetchedOptionData, _meta, _arg) => {
        // filter the differents categories and put in an array  (categories: string[])
        // transform the data and return  an OptionRowTransformed[] type (list: OptionRowData[])
        // will keep only option group present in the model options list
        let optionsGroupFiltered: CategoryRank = []
        let optionsKeys = Object.keys(res.options) as string[]

        const result : OptionRowTransformed[] = optionsKeys.map(key => {
          let constraint: Constraint | undefined
          optionsGroupFiltered = [... new Set([...optionsGroupFiltered, res.options[key].gp_opt])]
          if (res.incompatibilityTable.hasOwnProperty(key)) {
             constraint = {...res.incompatibilityTable[key]}
             if (constraint.incomp){
              constraint.incomp = [...new Set(constraint.incomp)]
             }
          }
          return { ref: key, constraint,  ...res.options[key]}
        })
        // remove group of options not present in  the options list
        optionsGroupFiltered = arrayIntersectUtils(res.categoryRank, optionsGroupFiltered)
        //result is of OptionsData[] type
        return {
          categoryRank: optionsGroupFiltered, 
          categoryOptions: res.categoryOptions,
          tmp: res.tmp,
          // incompatibilityTable: filteredIncompatibilityTable,
          list: result 
        } 
      }),
    }),

    /**
     * fetching options by model reference and group
     */
    getSavedConfigurationByUser: builder.query<SavedConfigurations, string>({
      // ajax_get_options.php?reference=MA00909&groupe=8
      query: (userId) => `${appConfig.api.savedConfigurationByUserPath}${userId}`,
      // res is the data fetched and we transform it to be of ModelsData[] type
      transformResponse: ((res: FetchedSavedConfigDataItem[], _meta, _arg) => {
        // filter the differents categories and put in an array  (categories: string[])
        let filteredSavedConfigurations: SavedConfigurations = []
        // filter response to get only name ids and and data parameters
        res.forEach(el => {
          if (el.id_save_conf && el.id_user &&  el.name && el.data) {
            try{
              // TODO: maybe need to parse el.data
                //console.log(el)
               const dataParsed = JSON.parse(el.data) as ConfigurationItems
              const filteredEl: SavedConfigDataItem = { id_user: el.id_user, id_save_conf: el.id_save_conf, name: el.name, conf: dataParsed,dateCreAt:el.dateCreAt,agent:el.agent,nom:el.nom,prenom:el.prenom}
              filteredSavedConfigurations = [...filteredSavedConfigurations, filteredEl]

            } catch(err){
              console.log('saved configuration data error')
              return;
            }
          }  
        })
        return filteredSavedConfigurations 
      }),
    }),
  }),
})

// Export hooks for usage in function components, which are
// auto-generated based on the defined endpoints
export const { 
  useGetUserJwtMutation, 
  useGetAllModelApiQuery, 
  useGetOptionsByModelApiQuery, 
  useSaveConfigurationMutation,
  useUpdateConfigurationMutation,
  useDeleteConfigurationMutation,
  useGetSavedConfigurationByUserQuery,
  useMakeOfferMutation
  } = apiSlice

export default apiSlice