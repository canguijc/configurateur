import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../rootReducer'

// Define a type for the slice state
// type PopupType = 'info' | 'warning' | 'configFinish' | 'confirm' | 'saveConf' | null

interface ModalState {
  isVisible: boolean
  id: string | undefined 
}


// Define the initial state using that type
const initialState: ModalState = {
 isVisible: false,
 id: undefined
}

export const modalPopupSlice = createSlice({
  name: 'modalPopup',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // payuload modal id reference
    showPopup: (state, action: PayloadAction<string>) => {
      state.isVisible = true
      state.id = action.payload
    },
    // when closing popup reset the popup state
    //  state.isVisible = false
    hidePopup: (state) => initialState, 
  },
})

export const { showPopup, hidePopup} = modalPopupSlice.actions
export const isModalVisible = (state: RootState) => state.modalPopup.isVisible
export const selectmodalId = (state: RootState) => state.modalPopup.id
// c' est le reducer a importer dans le store
export default modalPopupSlice.reducer