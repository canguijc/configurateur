import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../rootReducer'
import appConfig, { Language } from '../../../appconfig'
import i18n from './i18n'

// Define a type for the slice state
export interface TranslateState {
  lang: Language 
}

// Define the initial state using that type
const initialState: TranslateState = {
  lang: appConfig.translation.defaultLanguage,
}

export const translateSlice = createSlice({
  name: 'translate',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    changeLanguage: (state, action: PayloadAction<Language>) => {
      state.lang = action.payload
      i18n.changeLanguage(action.payload)
    },
  },
})

// Actions creator
export const {changeLanguage} = translateSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const selectCurrentLanguage = (state: RootState) => state.translate.lang

// reducer to import in store
export default translateSlice.reducer