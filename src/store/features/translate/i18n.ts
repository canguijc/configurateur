import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import config from "../../../appconfig" 
import Backend from "i18next-http-backend";

const defaultLanguage = config.translation.defaultLanguage    

const i18nOptions = {
    // no debug after compiling
    debug: import.meta.env.PROD ? false : true,
    lng:defaultLanguage,
    ns:'translation',
    interpolation: {
        escapeValue: false
    },
    backend:{ 
        // lng is 'fr' or 'de' test with this url type
         loadPath:  `${config.translation.baseTranslationUrl}?tradv2=1&lang={{lng}}`,
        // loadPath: '/locales/{{lng}}/{{ns}}.json',
        }
}
 
i18n
    .use(Backend)
    .use(initReactI18next)
    .init(i18nOptions)

export default i18n