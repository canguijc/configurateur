import { clearEngineSettings } from "../features/engineSetting/engineSettingSlice"
import { hidePopup } from "../features/modalpopup/modalPopupSlice"
import { clearRetrieveConfiguration } from "../features/retrieveConfiguration/retrieveConfigurationSlice"
import { clearSaveConfiguration } from "../features/saveConfiguration/saveConfigurationSlice"
import { useAppDispatch } from "./reduxHooks"
import { store } from "../store"
import { clearGamme } from "../features/gamme/gammeSlice"
import { clearFinancing } from "../features/financing/financingSlice"

const useAppClearStore = () => {
    return () =>{
        const dispatch = store.dispatch 
        dispatch(clearEngineSettings())
        dispatch(clearRetrieveConfiguration())
        dispatch(clearSaveConfiguration())
        dispatch(clearGamme())
        dispatch(hidePopup())
        dispatch(clearFinancing())
    }
}

export default useAppClearStore