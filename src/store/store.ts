import { configureStore } from '@reduxjs/toolkit'
import rootReducer from './rootReducer'
import apiSlice from './features/api/apiSlice'
import { FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE } from 'redux-persist'

export const store = configureStore({
  reducer: rootReducer,
  middleware:((getDefaultMiddleware) => 
    getDefaultMiddleware(
          {
            serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
          }
        ).concat([apiSlice.middleware])
      )
})

// Infer the `RootState` and `AppDispatch` types from the store itself
// export type RootState = ReturnType<typeof store.getState>

// Inferred type: {counter: counterState}
export type AppDispatch = typeof store.dispatch