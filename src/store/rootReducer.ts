// src/app/rootReducer.ts
import { combineReducers } from '@reduxjs/toolkit'
import translateSlice from './features/translate/translateSlice'
import gammeSlice from './features/gamme/gammeSlice'
import modalPopupSlice from './features/modalpopup/modalPopupSlice'
import apiSlice from './features/api/apiSlice'
import engineSettingSlice from './features/engineSetting/engineSettingSlice'
import retrieveConfigurationSlice from './features/retrieveConfiguration/retrieveConfigurationSlice'
import saveConfigurationSlice from './features/saveConfiguration/saveConfigurationSlice'
import deleteConfigurationSlice from './features/deleteConfiguration/deleteConfigurationSlice'
import financingSlice from './features/financing/financingSlice'

const rootReducer = combineReducers({
    [apiSlice.reducerPath]: apiSlice.reducer,
    engineSettings: engineSettingSlice,
    retrieveConfiguration: retrieveConfigurationSlice,
    deleteConfiguration: deleteConfigurationSlice,
    saveConfiguration: saveConfigurationSlice, 
    financing: financingSlice,
    translate: translateSlice,
    gamme: gammeSlice,
    modalPopup: modalPopupSlice
})

export type RootState = ReturnType<typeof rootReducer>

export  default rootReducer