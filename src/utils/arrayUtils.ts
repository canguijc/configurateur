/** intersection of 2 arrays.  return  values that are in both of them  
 *  keep the order  of elements defined in refArray
 * 
 * @param refArray  Reference array 
 * @param unorderedArray  array to compare
 * @returns  string or number array  
 */
export const arrayIntersectUtils = (refArray: string [], unorderedArray: string [] ): string [] => {
    return  refArray.filter(refArrayEl => unorderedArray.includes(refArrayEl)) 
} 

/** union of 2 arrays. return an array that contains  values  of both array
 *  without duplicated values
 * 
 * @param arr1 array of string or number  
 * @param arr2 array of string or number 
 * @returns  unique element in arr1 and arr2  
 */
export const arrayConcatWithoutDuplicateUtils = <T extends Number | string>(arr1: T[], arr2: T[]): T[] => {
    return [ ... new Set([...arr1, ...arr2])]
}

export const arrayfindNextItemUtils = <T>(dataArray:T[] , currentEl: T): T => {
    const currentItemIndex = dataArray.findIndex(el => el == currentEl)
    // next item if not the last else return the first element of dataArray 
    const nextItemIndex =  (currentItemIndex + 1) % dataArray.length
    return dataArray[nextItemIndex]
} 


/** return the step  of current category in category order array
 * 
 * @param arr  string[]  or number[] to search in array 
 * @param str  string  or number to search in array 
 * @returns  null if not found else is index +1 in arr 
 */
export  const arrayFindRankingUtils = <T extends number | string>(arr: T[], str: T): number | null => {
    const strIndex = arr.indexOf(str) 
    return  (strIndex > -1 ? (strIndex + 1) : null) 
 }

