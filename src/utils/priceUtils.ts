
export const calcPriceTTC = (htPrice: number, vat: number) => {
    const ttc = Math.ceil(htPrice  + (htPrice * vat/100))
    return ttc 
}

/*
function numberWithSpaces(x:number) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}
*/

export const formatPrice = (htPrice: number): string => {
    // const str =  Math.ceil(htPrice).toLocaleString('fr-FR', { minimumFractionDigits:0, maximumFractionDigits:0})
     const str =  htPrice.toLocaleString('ru-RU', { minimumFractionDigits:0, maximumFractionDigits:0})
     return `${str}.--`

}