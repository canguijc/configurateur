export const jwtPayloadDecode = (jwtString: string) => {
   try {
    const jwtData = jwtString.split('.') 
    return JSON.parse(atob(jwtData[1]))
   }
   catch(err) {
    throw new Error('jwt payload not valid')
   }
}

export const jwtPayloadStringified = (jwtString: string) => {
   try {
    const jwtData = jwtString.split('.') 
    return atob(jwtData[1])
   }
   catch(err) {
    throw new Error('jwt payload not valid')
   }
}
