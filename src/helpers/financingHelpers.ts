/*
if(tc==1 || tc==2)
    {
	 
      var t;
      var n=v_NbAn;

      if(v_taux!=0)
	  { t=v_taux/1200;
	    t=(t/(1-Math.pow(1+t, -n)));
	  }
      else{ t=1/n;
	  }
      if(tc==1)
	  { v_mens=Math.round((v_pret*t)*100)/100;
	  
	    //f.mensual.value=FormateVal(v_mens, 2, 0x0000);
	   var mens = FormateVal(v_mens, 2, 0x0000);
	  
	   if (mens == 'Inf ini,ty' || mens == ''){
		   $('#mensual').text('0,00');
	   }else{
		   $('#mensual').text(mens);
	   }
	  }
	  
      else{ v_pret=Math.round(v_mens/t);
	    f.pret.value=FormateVal(v_pret, 0, 0x0000);
	  }
	  
    }
*/


/** calculate mensual price of a credit
 *  for a given financial input at a given rate and a given duration

 * @param amount   amount to borrow 
 * @param nbMonth credit duration 
 * @param rate	credit rate 
 * @returns undefined if arguments are not numbers else  mensuality amount
 */
export const calcMensuality = (amount: number, nbMonth: number, rate: number, apportInt : number,echeanceInt:number): number | undefined =>  {
	if (typeof amount !== 'number' || typeof nbMonth !== 'number' || typeof rate !== 'number' || typeof apportInt !== 'number' ||
		typeof echeanceInt !== 'number' || amount==0 || nbMonth==0 || rate==0 || apportInt==0 || echeanceInt==0  ){
		return
	}
	/*
	maintenant il y a un apport donc le calcul est modifié :
	- il faut que la puissance soit nbMonth-1
	- on soustrait l'apport de l'amount
	 */
	amount=amount-apportInt;
	nbMonth=nbMonth*12;

	let t = 0
	if (rate !== 0) {
		t=rate/1200;
	    t=(t/(1-Math.pow(1+t, - (nbMonth-1))));
	}
      else{ 
		t=1/nbMonth;
	  }
	let mensualityPrice=Math.round((amount*t)*100)/100;
      mensualityPrice=mensualityPrice*echeanceInt;
	return  mensualityPrice
} 