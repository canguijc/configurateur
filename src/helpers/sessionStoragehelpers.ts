import appConfig from "../appconfig"

const sessionStorageUserKey = appConfig.sessionStorage.userKey

/**   get username in session
 * 
 * @returns  string | undefined
 */
export const getUserNameInSessionStoragehelper = (): string | undefined => {
    const user = sessionStorage.getItem(sessionStorageUserKey)
    if (!user) return;
    try  {
        const data = JSON.parse(user)['data']
        return JSON.parse(data)['nom']

    } catch (err){
        console.log('error parsing data ')
        return;
    }
}

/**   get userId in session
 * 
 * @returns  string | undefined
 */
export const getUserIdInSessionStoragehelper = (): string | undefined => {
    const user = sessionStorage.getItem(sessionStorageUserKey)
    if (!user) return;
    try  {
        const data = JSON.parse(user)['data']
        return JSON.parse(data)['id_user']
    } catch (err){
        console.log('error parsing data ')
        return;
    }
}

/**   get jwt in session
 * 
 * @returns  string | undefined
 */
export const getJwtInSessionHelper =  (): string | undefined => {
    const user = sessionStorage.getItem(sessionStorageUserKey)
    if (!user) {
        console.log('no user jwt')
        return;
    }
    try  {
        const username = JSON.parse(user)['jwt']
        return username

    } catch (err){
        console.log('error parsing data ')
        return;
    }
}

export const getUserProfil = async (): Promise<string | undefined> => {
    const user = await sessionStorage.getItem(sessionStorageUserKey)
    if (!user) {
        console.log('no user gup')
        return;
    }
    try {
        const data = JSON.parse(user)['data']
        return JSON.parse(data)['profil']
    } catch (err) {
        console.log('error parsing data ')
        return;
    }
}
export const getUserRole = async (): Promise<string | undefined> => {
    const user = await sessionStorage.getItem(sessionStorageUserKey)
    if (!user) {
        console.log('no user gur')
        return;
    }
    try {
        const data = JSON.parse(user)['data']
        return JSON.parse(data)['role']
    } catch (err) {
        console.log('error parsing data ')
        return;
    }
}
