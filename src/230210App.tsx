import { BrowserRouter, Link, Route, Routes } from 'react-router-dom'

import Navbar from './components/navbar/Navbar'
import ModelList from './pages/configurationPage/modelList/ModelList'
import ConfigurationPage from './pages/configurationPage/ConfigurationPage'
import GammePage from './pages/gammePage/GammePage'


// style
import './scss/custom-bootstrap.scss'
import 'bootstrap'
import './App.scss'
import OptionServiceList from './pages/configurationPage/optionServiceList/OptionServiceList'
import User from './components/user/User'
import {Suspense, useEffect, useState} from 'react'
import SpinnerFullScreen from './components/ui/spinner/spinnerFullScreen/SpinnerFullScreen'
import NoMatchPage from './pages/noMatchPage/NoMatchPage'
import TestMode from './components/testMode/TestMode'
import ConfigurationStep from './components/configurationStep/ConfigurationStep'
import appConfig from './appconfig'
import ResumePage from './pages/resumePage/ResumePage'
import ConfirmationModal from './components/confirmationModal/ConfirmationModal'
import RetrieveConfigurationPage from './pages/retrieveConfigurationPage/RetrieveconfigurationPage'
import {getUserProfil, getUserRole} from "./helpers/sessionStoragehelpers";


// configure the base RootPath for  react-router-dom if App is hosted under a subdirectory (eg: /configurateur)
// can change the subdirectory in env.production file
const baseRouterPath= import.meta.env.VITE_ROUTER_BASE_PATH
const pages = appConfig.pages


function App() {
  const [role,setRole]=useState("");
  const [type,setType]=useState("");
  const [active,setActive]=useState(false);
  async function fetchData() {
    const data1 = await  getUserProfil().then( async r=>{
          //console.log(r.data)
          // @ts-ignore
          //setType(r)
          return r
        }
    );
    const data2 = await  getUserRole().then(async r=>{
      // @ts-ignore
      //setRole(r)
      return r
    });
    return Promise.all([data1, data2]).then(([data1, data2]) => {
      console.log(data1,data2)
      if(data2=="ROLE_RESPONSABLE_VENTES" || data2=="ROLE_ADMIN" || (data2=="ROLE_AGENT" && data1=='1')){

      setActive(true);
      }
      // setAllData(categorizeFiches(data2,data1));

    });
  }
//role=="ROLE_RESPONSABLE_VENTES" || role=="ROLE_ADMIN" || (role=="ROLE_AGENT" && type=='1'
  useEffect(() => {

    fetchData().then(async r=>{

      // setActive(true)
    })

    //setAllData(categorizeFiches(categories,fiches));

  }, []);



  return (
    <div className="App mb-3">
      <Suspense fallback={<div className="w-100 h-100 m-auto "><SpinnerFullScreen/></div>}>
        <User>
          <BrowserRouter basename={baseRouterPath}>
            <TestMode/>
            <ConfirmationModal/>

            <Navbar   active={active}/>
            <ConfigurationStep/>
            <Routes>
              <Route path={pages.gammes.link.abs} element={<GammePage/> }/>  
              <Route path ={pages.config.link.rel} element={<ConfigurationPage/>}>
                <Route path={pages.models.link.rel} element={<ModelList/>}/>
                <Route path={pages.options.link.rel} element={<OptionServiceList/>}/>
              </Route>
              <Route path={pages.resume.link.rel} element={<ResumePage/>}/>
              <Route path={pages.retrieveConfig.link.rel} element={<RetrieveConfigurationPage/>}/>
              <Route path='*' element={<NoMatchPage/> }/>  
            </Routes>
          </BrowserRouter>
        </User>
      </Suspense>
    </div>
  )
}

export default App
